# Dependencies
FROM node:18-alpine AS dependencies
RUN apk --no-cache add git
WORKDIR /usr/src/app
COPY package.json /usr/src/app
RUN npm install

# Build
FROM dependencies as build
COPY . /usr/src/app
RUN npm run build

# Run
FROM nginx
COPY ./default.conf /etc/nginx/conf.d/default.conf
COPY --from=build /usr/src/app/dist/private-routes /usr/share/nginx/html
