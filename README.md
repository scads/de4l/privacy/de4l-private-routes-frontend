# Privacy Tuna Frontend

Angular application to visualize trajectory data, assess its privacy risk and utility, and to tune privacy-preserving mechanisms to make the data private but keep good utility.
Leaflet.js is used for map visualizations. 
The data is fetched via REST from the [*Privacy Tuna*] [SpringBoot backend](https://git.informatik.uni-leipzig.de/scads/de4l/privacy/de4l-private-routes-backend). The calculation of privacy-preservation and privacy and utility metrics is requested from a second [*Privacy Tuna*] [Flask backend](https://git.informatik.uni-leipzig.de/scads/de4l/privacy/de4l-privacy-services).

## Development
To run the development server, you need [nodejs](https://nodejs.org/), [npm](https://www.npmjs.com/) and the [angular cli](https://angular.io/).
The recommended way of installing nodejs and npm is via a node version manager e.g. [nvm](https://github.com/nvm-sh/nvm#installing-and-updating).
If nodejs and npm are installed, you can install the angular cli with:
```bash
npm install -g @angular/cli
```

Change into the directory of the repository and install the dependencies with:
```bash 
cd de4l-private-routes-frontend
npm install
```

Now you can run the frontend in development-mode with:
```bash
ng serve --host 0.0.0.0
```
The Angular Live Development Server is now listening on [127.0.0.0.1:4200](http://127.0.0.1:4200/) with live-reload enabled.
To continue the development of the *Privacy Tuna* application, also check the [SpringBoot backend](https://git.informatik.uni-leipzig.de/scads/de4l/privacy/de4l-private-routes-backend) and the [Flask backend](https://git.informatik.uni-leipzig.de/scads/de4l/privacy/de4l-privacy-services).

## Build
The [*Privacy Tuna*](https://github.com/majaschneider/privacytuna) application requires the frontend to
run in a docker container. Build the container with:
```bash
docker build -t private-routes-frontend:latest .
```
The image contains a customized nginx configuration with an additional endpoint for a healthcheck and a reverse proxy to the backend.
(The nginx config file is default.conf)
