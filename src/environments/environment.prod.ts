export const environment = {
  production: true,
  keycloak: false,
  importWindowOnStartup: false,
  helpDialogOnStartup: true,
  import: true,
  export: true,
  editRoutes: true
};
