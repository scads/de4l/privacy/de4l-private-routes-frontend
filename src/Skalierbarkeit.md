#Skalierbarkeit 
Das Frontend kann jetzt ungefähr 5000 Punkte gut verarbeiten. ( Noise2D funktioniert allerdings nur bis ca. 2000 Punkte.)
Die Anzahl der gleichzeitig geladenen, unprivatisierten Punkte, muss auf 2000-5000 beschränkt werden. Ansonsten funktioniert das Privatisieren nicht mehr schnell genug und die Seite wird insgesamt zu langsam.

Für die Anzige auf der Karte muss man die Daten also irgendwie aussortieren / clustern, sodass nur nötige Punkte geladen werden.
Nötig heißt: Die Punkte geben eine gute Übersicht und sind gleichzeitig detailliert genug, um die Effekte der Privatisierungsalgorithmen abzubilden.

Der Beispieldatensatz beinhaltete nur Punkte innerhalb von Porto, dh. die Dichte war relativ groß und man hatte schon bei mittleren Zoomlevels den gesamten Datensatz in der BoundingBox.
Wahrscheinlich sind die meisten Datensätze in dieser Hinsicht ähnlich.

## Lösungsvorschlag
Es sollen nicht alle Punkte gleichzeigt aus dem Backend geholt werden, sondern immer nur die Punkte aus der aktuellen BoundingBox.
(BoundingBox := der derzeitig sichtbare Ausschnitt auf der Karte im Frontend).
Nur diese Punkte werden auch privatisiert und in der rechten Karte angezeigt.

### Ein beispielhafter Ablauf:
Karte wird im Frontend wird verschoben, neue BoundingBox = B¹, Algorithmus = Noise2D, Parameter = 0.023
 |
Anfrage des Frontends an das Backend: getPointsInBoundingBox(B¹)
 |
Backend holt Punkte aus der Datenbank und clustert, sortiert aus, interpoliert, ... , sodass es am Ende maximal 5000 Punkte sind. (1)
 |
Backend schickt Liste der Punkte ans Frontend.
 |
Frontend berechnet Visualisierungen der Punkte und zeigt sie in der linken Karte an
 |
Anfrage des Frontends an das Backend: getPrivatizedPointsInBoundingBox(B¹, Noise2D, 0.023)
 |
Backend holt Punkte aus der Datenbank und clustert, sortiert aus, interpoliert, ... , sodass es am Ende maximal 500 Punkte sind und sie dennoch sinnvoll privatisiert werden können (2)
 |
Backend kommuniziert mit Privatisierungsbackend und holt die privatisierten Punkte/Routen
 |
Backend schickt Liste der Punkte ans Frontend.
 |
Frontend berechnet Visualisierungen der Punkte und zeigt sie in der rechten Karte an

### Zusätzliche Endpoints
Da dann nicht mehr der gesamte Datensatz im Frontend liegen würde, bräuchte man auch noch folgende zusatzliche Endpunkte im Backend:
-   getMetricForRoute( routeID, metric ) -> Utility- und Privacywerte für die ausgewählte Route und Metrik
-   getGlobalMetric() -> Utility- und Privacywerte für den gesamten Datensatz
-   getRoutesOverview() -> Liste der Routen (ohne die einzelnen Punkte), mit Metadaten zum Überblick, z.b. wie viele Punkte sind in der Route, wie viele Stops, wo liegt die Route, etc. Zur Anzeige der Routenliste in der Tabellenansicht.


## Aussortiern / Clustern der Daten
Die Frage, an der alles hängt ist: Was passiert genau im Beispielablauf bei (1) und (2)?

### Lösungsansätze für (1)

#### Datenbank
-   Alle Punkte aus der Datenbank ins Backend laden und dort alle Punkte aussortieren, die außerhalb der BoundingBox liegen.
		Dabei muss die gesamte Datenbank in den Hauptspeicher geladen werden, das funktioniert nur bis zu einer gewissen Größe.
-   Auf Datenbank mit Geodatenunterstützung wechseln, sodass man per Query alle Punkte innerhalb einer BoundingBox anfragen kann.
-   Pro Route zusätzliche Metadaten in der Datenbank speichern. Z.B. max_latitude, min_latitude, max_longitude, min_longitude. Dann könnte man per Query nur Routen innerhalb einer BoundingBox anfragen.

Probleme:
- 	Es werden auch Routen geladen, die nur einen einzigen Punkt innerhalb der BoundingBox haben.
- 	Einzelne Routen können bereits mehr als 5000 Punkte haben.
-   BoundingBox ist gesamte Welt --> alle Punkte werden geladen.

Die Datenbankanfrage nach BoundingBox bietet keine Lösung des Skalierungsproblems. Bringt aber trotzdem Geschwindigkeitsvorteile bei mittleren und nahen Zoomlevels.
Nach der Datenbankanfrage müssen die Routen deswegen noch vereinfacht werden, damit sichergestellt ist, dass 5000 Punkte nicht überschritten werden.

## Zusätzliche Optimierungen

## Visualisierungstypen
Für jeden Visualisierungstyp (Lines, Dots, LinesWithDots, Heatmap) werden Leafletmarker berechnet und im Speicher gehalten.
Dies geschieht beim ersten Laden der Routendaten vom Server.
Dh. wenn 500 unprivatisierte Punkte geladen werden, werden 4 * 500 Marker für die linke Karte berechnet und dazu kommen die 4 * 500 Marker der rechten Karte.

Lösungsvorschläge:
-   Auf einige Visualisierungstypen verzichten. z.b. nur Lines und Dots.
- 	Nur eine Karte gleichzeitig anzeigen, entweder private ODER öffentliche Daten.
-   Die Visualisierungen in dem Moment berechnen, in dem sie ausgewählt werden. Weniger Marker müssten gleichzeitig im Speicher gehalten werden,
		aber unter Umständen dauert das Wechseln der Visualisierungstypen länger und man muss Marker mehrfach berechnen, wenn man z.b. von Lines -> Dots -> Lines switched.
-   Visulisierungen im Backend berechnen und z.B. als GeoJson ans Frontend schicken. Im GET-Points-Request des Frontends, müsste man den aktuellen Visualisierungstyp angeben. Das Frontend müsste dann keine Marker mehr berechnen -> schnellste Variante.


## Notizen nach dem Treffen mit Martin
- Postgreserweiterung für Geodaten - PostGIS https://www.postgis.net/
	Bietet Datentypen zu Repräsentation von Geodaten aller Art.
	https://www.postgis.net/docs/manual-3.2/using_postgis_dbmanagement.html#Point

	Bietet Indizierung von Geodatens. Damit ist schnelles Retrieval von Geodaten, z.b. durch Angabe einer Bounding Box, möglich.
	https://postgis.net/workshops/postgis-intro/indexing.html

	Bietet verschiedene Funktionen für Berechnungen auf Geodaten. Darunter auch drei Funktionen zum Vereinfachen von Routendaten:
	Douglas-Peucker algorithm https://postgis.net/docs/manual-3.2/ST_Simplify.html
	Visvalingam-Whyatt algorithm https://postgis.net/docs/manual-3.2/ST_SimplifyVW.html
 	Chaikins-Algorithm https://postgis.net/docs/manual-3.2/ST_ChaikinSmoothing.html
	und zur Interpolation: https://postgis.net/docs/manual-3.2/ST_LineInterpolatePoints.html

	Liesße sich einfach in das bestehende Backend integrieren, da mit PostGIS keine Funktionalitäten der bestehenden Postgresdatenbank verloren gehen. Der Unterschied wird sein, dass wir die Geodaten in PostGIS-Datentypen speichern und in den Queries PostGIS Befehle verwenden. 

	Gibts auch als Docker-Container
	https://registry.hub.docker.com/r/postgis/postgis/


- Geschwindigkeitsmessung. Wo genau sind die Bottlenecks?
	- Funktionsaufrufe in Two-Way-Bindings im HTML-Code.
		etwas wie {{ selection.getAllSelectedRoutes() }} wird bei jeder Interaktion mit dem Frontend erneut aufgerufen. Dadurch müssen Funktionen mehrfach berechnent werden, auch wenn sich gar nichts geändert hat.
		Lösung: Verwenden von Variablen, die das Ergebnis der Funktion enthalten. Ändern sich die Daten, muss die Funktion nur einmal neu berechnet und die Variable aktualisiert werden.
		-->  {{ selectedRoutes }}

	- addMetricToGeodata() muss pro Punkt über jede vorhandene Metrik iterieren.
		Bei 5000 Punkten und 2 Metriken, sind das 2 * ( 5000 * 5000 ) Schritte
		Die Lösung könnte sein, vom Backend Routen INKLUSIVE der Metriken anzufragen, anstatt alles einzeln.

	- Überlappende Punkte.
		Dadurch müssen auf der Karte Pixel gezeichnet werden, die man gar nicht sehen kann.
		Lösung: Kleineren Punktdurchmesser wählen.

	- Tabellenansicht.
		Beim Öffnen des Tabellentabs werden auch alle (unsichtbaren) Punktetabellen berechnet.
		Lösung: Berechne Punktetabellen nur wenn sie auch ausgeklappt ist

	- Zusätzliche Möglichkeit: WebWorkers bei aufwändigen Berechnungen verwenden. Dann 	 wird die UI nicht blockiert.

- Trajectoryclustering
			Es gibt vorhandene Algorithmen, mit denen man Routen vereinfachen kann. Damit lässt sich das Problem der Darstellung der Routendatenverläufe gut lösen. Wenn man weit rausgezoomt hat, kann man sowieso nicht alle Details erkennen. Das eigentliche Problem wird sein, einen Algorithmus zu finden/entwerfen, der auch die Metriken sinnvoll vereinfacht. Eine problematische Stelle in der Route muss auch nach dem Clustern noch erkennbar sein, damit man weiß, an welcher Stelle es sich lohnt reinzuzoomen.

- Elasticsearch als Datenbank
		Hat am Ende ähnlich viele Möglichkeiten mit Geodaten umzugehen, wie PostGIS, aber wäre viel Aufwändiger umzusetzen. (Anderer Datenbanktyp --> Änderungen an unserem Datenmodell; Weniger SpringBoot/Hibernate Unterstützung --> man müsste viel mehr Code schreiben).

- Noise2D untersuchen nach Bottlenecks

## PostGIS
### Tabellen
-   route
			id, name, date
-   point
			id, location, stop, timestamp, route_id
			location ist vom geography Datentyp und enthält POINT ( latitude, longitude )
### Datenzugriff
-   Alle Routen mit Geodaten als LINESTRING
```
SELECT p.route_id, r."name", ST_makeLine( p.geom_point order by p.id ) FROM public.point  as p  join public.route as r on r.id = p.route_id  group by p.route_id , r."name";
```
-   Auf das Ergebnis von ST_makeLine kann man dann ST_Simplify anwenden, um die Route zu vereinfachen


SELECT route_id, ST_makeLine( geom_point order by id ) as route FROM public.point group by route_id ;
