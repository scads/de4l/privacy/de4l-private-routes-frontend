import { Component } from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';
import { SafeUrl } from '@angular/platform-browser';
import saveAs from 'file-saver';
import { merge, map, Observable, reduce, from, of, toArray } from 'rxjs';
import { Route } from 'src/app/models/route';
import { CommunicationService } from 'src/app/services/communication.service';
import { PrivacyService } from 'src/app/services/privacy.service';
import { MetricService } from 'src/app/services/metric.service';
import { RouteOverviewSelectionService } from 'src/app/services/selection/route-overview-selection.service';

@Component({
  selector: 'app-export-data',
  templateUrl: './export-data.component.html',
  styleUrls: ['./export-data.component.css']
})
export class ExportDataComponent {

  options = this._formBuilder.group({
    protectedData: true,
    publicData: true,
    metrics: true
  });

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private communication: CommunicationService,
    private selection: RouteOverviewSelectionService,
    private privacyService: PrivacyService,
    private metricService: MetricService
  ) { }

  valid(): boolean {
    return this.options.value.publicData ||
      (this.options.value.protectedData && this.privacyService.currentSetting.getValue() != null)
  }

  /**
   * Converts the route to a format suitable for export.
   * Omits unnecessary properties
   */
  private routeToExportRoute(route: Route, options: any) {
    let p = route.points.map(p => {
      let pointData = {
        id: p.id,
        latitude: p.latitude,
        longitude: p.longitude,
        timestamp: p.timestamp,
        measurements: p.measurements,
      };
      if (options.value.metrics) {
        let pointDataWithMetrics = {
          ...pointData,
          metrics: p.metrics.map(({ id, ...remainder }) => remainder)
        };
        return pointDataWithMetrics;
      }
      return pointData;
    });

    let routeData = {
      id: route.id,
      name: route.name,
      date: route.date,
      points: p
    };
    if (options.value.metrics) {
      let routeDataWithMetrics = {
        ...routeData,
        metrics: route.metrics.map(({ id, ...remainder }) => remainder)
      };
      return routeDataWithMetrics;
    }
    return routeData;
  }

  private preparePrivateDataDownload(options: any) {
    let currentSetting = this.privacyService.currentSetting.getValue()
    if (currentSetting == null) {
      return of({ protectedData: [] })
    }
    let protectedDataPromise = this.communication.getPrivateData(
      this.selection.currentlySelectedIds(),
      currentSetting.algorithm.id,
      currentSetting.parameters,
      this.metricService.currentPrivacyMetrics.concat(this.metricService.currentUtilityMetrics).map(metric => {return metric.id})
    )
    if (options.value.metrics) {
      return from(protectedDataPromise).pipe(
        map(r => { return {
          protectedData: {
            routes: r.routes.map(rs => this.routeToExportRoute(rs, options)),
            metrics: r.metrics.map(({ id, ...remainder }) => remainder)
          }
        }})
      )
    }
    let protectedDataObservable = from(protectedDataPromise).pipe(
      map(r => { return {
        protectedData: {
          routes: r.routes.map(rs => this.routeToExportRoute(rs, options))
        }
      } })
    )
    return protectedDataObservable
  }

  private preparePublicDataDownload(options: any) {
    let publicDataPromise = this.communication.getPublicData(
      this.selection.currentlySelectedIds(),
      this.metricService.currentPrivacyMetrics.concat(this.metricService.currentUtilityMetrics).map(metric => {return metric.id})
    )
    if (options.value.metrics) {
      return from(publicDataPromise).pipe(
        map(r => { return {
          publicData: {
            routes: r.routes.map(rs => this.routeToExportRoute(rs, options)),
            metrics: r.metrics.map(({ id, ...remainder }) => remainder)
          }
        } })
      )
    }
    return from(publicDataPromise).pipe(
      map(r => { return {
        publicData: {
          routes: r.routes.map(rs => this.routeToExportRoute(rs, options))
        }
      } })
    )
  }

  public downloadData() {
    const observables: Observable<any>[] = []
    if (this.options.get('protectedData')?.value) {
      observables.push(this.preparePrivateDataDownload(this.options))
      observables.push(of(this.privacyService.currentSetting.getValue()))
    }
    if (this.options.get('publicData')?.value) {
      observables.push(this.preparePublicDataDownload(this.options))
    }
    merge(...observables).pipe(
      reduce((acc, val) => { return { ...acc, ...val } }, {} as any),
    ).subscribe((data: any) => {
      this.createDownloadUrl(data)
    })
  }

  private createDownloadUrl(data: any): SafeUrl {
    const blob = new Blob([JSON.stringify(data, undefined, 2)], { type: "application/json" })
    saveAs(blob, this.createFilename())
    const url = URL.createObjectURL(blob)
    return url
  }

  private createFilename(): string {
    const currentDate = new Date();
    const formattedDate = currentDate.toLocaleDateString();
    const formattedTime = currentDate.toLocaleTimeString([],
        { hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false });
    return `privacy-tuna-export_${formattedDate}_${formattedTime}.json`
  }

}
