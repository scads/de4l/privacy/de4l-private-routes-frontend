import { Component, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { concat, Subject, takeUntil, tap } from 'rxjs';
import { RouteOverview } from 'src/app/models/route-overview';
import { CommunicationService } from 'src/app/services/communication.service';
import { RouteOverviewService } from 'src/app/services/route-overview.service';
import { RouteOverviewSelectionService } from 'src/app/services/selection/route-overview-selection.service';

@Component({
  selector: 'app-delete-route-dialog',
  templateUrl: './delete-route-dialog.component.html',
  styleUrls: ['./delete-route-dialog.component.css']
})
export class DeleteRouteDialogComponent implements OnDestroy {

  loading = false
  destroy = new Subject<boolean>()

  constructor(
    public dialogRef: MatDialogRef<DeleteRouteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public routeOverviewsToDelete: RouteOverview[],
    public communication: CommunicationService,
    public selection: RouteOverviewSelectionService,
    public routeOverviews: RouteOverviewService
  ) { }

  ngOnDestroy(): void {
    this.destroy.next(true)
    this.destroy.complete()
  }

  idsString(): string {
    return this.routeOverviewsToDelete.map(r => r.id).toString()
  }

  deleteRouteBtnClick() {
    this.loading = true
    const observer = this.routeOverviewsToDelete.map(ro => this.communication.deleteRoute(ro.id))
    concat(...observer).pipe(
      takeUntil(this.destroy)
    ).subscribe({
      complete: () => {
        this.routeOverviews.updateRouteOverviews()
        setTimeout(() => {
          this.dialogRef.close()
        }, 500
        )
      }
    })
  }

}
