import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RouteOverview } from 'src/app/models/route-overview';
import { CommunicationService } from 'src/app/services/communication.service';
import { RouteOverviewService } from 'src/app/services/route-overview.service';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-drop-points-dialog',
  templateUrl: './drop-points-dialog.component.html',
  styleUrls: ['./drop-points-dialog.component.css']
})
export class DropPointsDialogComponent {

  public everyNth: number
  public max: number
  public loading = false

  constructor(
    public dialogRef: MatDialogRef<DropPointsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public routeOverview: RouteOverview[],
    public communication: CommunicationService,
    public routeOverviewService: RouteOverviewService

  ) { 
    this.max = routeOverview.map( r => r.pointCount ).reduce( (a,b) => a > b ? a : b ) 
    this.everyNth = Math.floor( this.max / 2 )
  }

  idsString(): string {
    return this.routeOverview.map( r => r.id ).toString()
  }

  dropPointsBtnClick(): void {
    this.loading = true
    let requests = this.routeOverview.map( ro => {
      return this.communication.dropPoints(ro.id, this.everyNth)
    })
    forkJoin( requests ).subscribe( (s) => {
      this.routeOverviewService.updateRouteOverviews();
      this.dialogRef.close()
    } )
  }

}
