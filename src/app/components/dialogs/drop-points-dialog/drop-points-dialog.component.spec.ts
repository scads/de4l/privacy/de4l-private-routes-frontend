import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DropPointsDialogComponent } from './drop-points-dialog.component';

describe('DropPointsDialogComponent', () => {
  let component: DropPointsDialogComponent;
  let fixture: ComponentFixture<DropPointsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DropPointsDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DropPointsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
