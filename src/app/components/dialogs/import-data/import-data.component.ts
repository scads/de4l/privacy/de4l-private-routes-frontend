import { MaxSizeValidator } from '@angular-material-components/file-input';
import { Component, OnInit } from '@angular/core';
import { FormControl, UntypedFormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RouteOverviewService } from 'src/app/services/route-overview.service';
import { ImportGenericService } from 'src/app/services/import/import-generic.service';
import { ImportService } from 'src/app/services/import/import.service';
import { ProgressBarService } from 'src/app/services/progress-bar.service';
import { RouteDataService } from 'src/app/services/route-data.service';
import { RouteToMapService } from 'src/app/services/route-to-map.service';
import { ImportFeedbackComponent } from '../import-feedback/import-feedback.component';

@Component({
  selector: 'app-import-data',
  templateUrl: './import-data.component.html',
  styleUrls: ['./import-data.component.css']
})
export class ImportDataComponent implements OnInit {

  fileControl: UntypedFormControl;
  public file!: File[]

  importServices: ImportService<any>[]
  importer: ImportService<any>

  progressBarVisible = false
  feedbackText = ""

  constructor(
    public dataService: RouteDataService,
    public visualizationService: RouteToMapService,
    public progressBar: ProgressBarService,
    public importGeneric: ImportGenericService,
    public dialogRef: MatDialogRef<ImportDataComponent>,
    public dialog: MatDialog,
    public routeOverviewService: RouteOverviewService,
    public snackBar: MatSnackBar
  ) {
    this.importServices = [
      importGeneric,
    ]
    // Starte mit dem ersten Importer aus der Liste
    this.importer = this.importServices[0]

    this.fileControl = new FormControl(this.file, [
      Validators.required,
      MaxSizeValidator(this.importer.maxFileSize)
    ])
  }

  ngOnInit(): void {
    this.fileControl.valueChanges.subscribe( () => { this.progressBarVisible = false })
  }

  importerSelectionChanged() {
    this.fileControl.setValue( [] )
  }

  uploadBtnClick(): void {
    this.progressBarVisible = true
    this.importer.parseFile( this.fileControl.value[0] ).subscribe( {
      next: (parsedFile) => {
        this.importer.importData(parsedFile).subscribe( responseArray => {
          this.dialog.open(ImportFeedbackComponent, {
            data: { 
              filename: this.fileControl.value[0].name,
              responseArray: responseArray
            }
          } )
          this.dialogRef.close()
          this.routeOverviewService.updateRouteOverviews()
        })
      },
      error: (e) => {
        this.snackBar.open( e, "Ok" )
        this.dialogRef.close()
      }
    })
    this.dialogRef.close()
  }

  public spinnerMode(): 'determinate' | 'indeterminate' {
    return this.progressBar.currentMode() == 'determinate' ? 'determinate' : 'indeterminate'
  }

}
