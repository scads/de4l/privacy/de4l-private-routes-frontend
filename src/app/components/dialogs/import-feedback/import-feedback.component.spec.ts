import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportFeedbackComponent } from './import-feedback.component';

describe('ImportFeedbackComponent', () => {
  let component: ImportFeedbackComponent;
  let fixture: ComponentFixture<ImportFeedbackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportFeedbackComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ImportFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
