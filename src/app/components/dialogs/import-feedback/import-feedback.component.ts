import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { VisibilityService } from 'src/app/services/visibility.service';
import { CommunicationService } from 'src/app/services/communication.service';

export interface FeedbackData {
  filename: string
  responseArray: (boolean | Object)[]
}

@Component({
  selector: 'app-import-feedback',
  templateUrl: './import-feedback.component.html',
  styleUrls: ['./import-feedback.component.css']
})
export class ImportFeedbackComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: FeedbackData,
    public matDialog: MatDialogRef<ImportFeedbackComponent>,
    public visibility: VisibilityService,
    public translate: TranslateService,
    public communicationService: CommunicationService
  ) { }

  numErrors(): number {
    return this.data.responseArray.length - this.data.responseArray.filter((e) => e === true).length
  }

  responseToStatus(resp: any | boolean): string {
    if (resp === true) {
      return this.translate.instant('ImportSucc')
    } else if (resp === false) {
      return this.translate.instant('ImportUnkownError')
    }
    if (resp.error.message) {
      return resp.error.message
    }
    return resp.error + " \"" + resp.message + "\" "
  }

  close() {
    this.visibility.drawerOpen = true
    this.matDialog.close()
  }

}
