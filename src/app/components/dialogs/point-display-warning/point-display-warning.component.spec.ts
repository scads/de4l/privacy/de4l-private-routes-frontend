import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PointDisplayWarningComponent } from './point-display-warning.component';

describe('PointDisplayWarningComponent', () => {
  let component: PointDisplayWarningComponent;
  let fixture: ComponentFixture<PointDisplayWarningComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PointDisplayWarningComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PointDisplayWarningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
