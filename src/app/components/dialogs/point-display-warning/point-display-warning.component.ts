import { Component, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { VisibilityService } from 'src/app/services/visibility.service';
import { RouteOverviewService } from 'src/app/services/route-overview.service';
import { RouteOverviewSelectionService } from 'src/app/services/selection/route-overview-selection.service';


@Component({
	selector: 'app-loading-error',
	templateUrl: './point-display-warning.component.html',
	styleUrls: ['./point-display-warning.component.css']
})
export class PointDisplayWarningComponent {

	constructor(
	    public dialogRef: MatDialogRef<PointDisplayWarningComponent>,
	    private routeOverviewService: RouteOverviewService,
	    public routeSelectionService: RouteOverviewSelectionService,
	    private visibility: VisibilityService,

    ) { }

    continueButtonClick() {
        this.routeOverviewService.displayedRoutes.next(this.routeSelectionService.selected.getValue())
        this.dialogRef.close()
        this.visibility.drawerOpen = false;
    }
}
