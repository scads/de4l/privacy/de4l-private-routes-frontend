import { Component, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { VisibilityService } from 'src/app/services/visibility.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
	selector: 'app-loading-error',
	templateUrl: './loading-error.component.html',
	styleUrls: ['./loading-error.component.css']
})
export class LoadingErrorComponent {

	constructor(
	    public dialogRef: MatDialogRef<LoadingErrorComponent>,
	    private translate: TranslateService,
    ) { }

}
