import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { IdwSetting } from 'src/app/services/idw-options.service';

@Component({
  selector: 'app-idw-options-form',
  templateUrl: './idw-options-form.component.html',
  styleUrls: ['./idw-options-form.component.css']
})
export class IdwOptionsFormComponent implements OnInit {

  @Input() idwSetting!: IdwSetting
  @Input() name!: string
  @Output() newSetting: EventEmitter<IdwSetting> = new EventEmitter()
  appearance: MatFormFieldAppearance = "outline"

  constructor() { }

  ngOnInit(): void {
  }

  confirm() {
    this.newSetting.emit(this.idwSetting)
  }
}
