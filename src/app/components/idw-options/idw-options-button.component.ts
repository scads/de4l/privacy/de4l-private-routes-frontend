import { AfterViewInit, Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { IdwOptionsComponent } from './idw-options.component';

@Component({
  selector: 'app-idw-options-button',
  templateUrl: './idw-options-button.component.html',
  styleUrls: ['./idw-options-button.component.css']
})
export class IdwOptionsButtonComponent implements AfterViewInit {

  constructor(
    private dialog: MatDialog
  ) { }

  ngAfterViewInit(): void {
  }

  click() {
    this.dialog.open(IdwOptionsComponent)
  }
}
