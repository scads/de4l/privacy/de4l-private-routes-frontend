import { Component, OnInit } from '@angular/core';
import { IdwOptionsService, IdwSetting } from 'src/app/services/idw-options.service';

@Component({
  selector: 'app-idw-options',
  templateUrl: './idw-options.component.html',
  styleUrls: ['./idw-options.component.css']
})
export class IdwOptionsComponent implements OnInit {

  constructor(
    private idwOptionsService: IdwOptionsService
  ) { }

  ngOnInit(): void {
  }

  options() {
    const currentMap = this.idwOptionsService.currentSettings.getValue()
    return currentMap
  }

  newSetting(name: string, $event: IdwSetting) {
    this.idwOptionsService.newSetting(name, $event)
  }

}
