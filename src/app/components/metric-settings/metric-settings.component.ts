import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MetricService } from 'src/app/services/metric.service';
import { MetricMetadata } from 'src/app/models/metric-metadata';
import { RouteDataService } from 'src/app/services/route-data.service';
import { RouteToMapService } from 'src/app/services/route-to-map.service';
import { ColoringService } from 'src/app/services/coloring.service';
import { FetchingDataService } from 'src/app/services/fetching-data.service';

@Component({
  selector: 'app-metric-settings',
  templateUrl: './metric-settings.component.html',
  styleUrls: ['./metric-settings.component.css']
})
export class MetricSettingsComponent {

    @Input() selectedMetric!: MetricMetadata;
    privacyMetrics = new FormControl([this.metricService.availablePrivacyMetrics[0]]);
    utilityMetrics = new FormControl([this.metricService.availableUtilityMetrics[0]]);

  constructor(
    public routeToMapService: RouteToMapService,
    public metricService: MetricService,
    public routeDataService: RouteDataService,
    public coloring: ColoringService,
    public fetchingDataService: FetchingDataService,
  ) {
  }

}

