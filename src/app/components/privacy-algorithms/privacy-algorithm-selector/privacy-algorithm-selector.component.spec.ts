import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivacyAlgorithmSelectorComponent } from './privacy-algorithm-selector.component';

describe('PrivacyAlgorithmSelectorComponent', () => {
  let component: PrivacyAlgorithmSelectorComponent;
  let fixture: ComponentFixture<PrivacyAlgorithmSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrivacyAlgorithmSelectorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PrivacyAlgorithmSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
