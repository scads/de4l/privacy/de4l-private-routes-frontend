import { Component, OnDestroy } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { PrivacyAlgorithmMetadata } from 'src/app/models/PrivacyAlgorithmMetadata';
import { PrivacyService } from 'src/app/services/privacy.service';
import { RouteOverviewSelectionService } from 'src/app/services/selection/route-overview-selection.service';

@Component({
  selector: 'app-privacy-algorithm-selector',
  templateUrl: './privacy-algorithm-selector.component.html',
  styleUrls: ['./privacy-algorithm-selector.component.css']
})
export class PrivacyAlgorithmSelectorComponent implements OnDestroy {

  availableAlgorithms: PrivacyAlgorithmMetadata[] = []
  selected: PrivacyAlgorithmMetadata | null = null
  disabled = true
  destroy = new Subject<boolean>()

  constructor(
    public privacyService: PrivacyService,
    public routeOverviewSelection: RouteOverviewSelectionService,
  ) {
    this.initSelected()
    privacyService.availableAlgorithms.pipe(
      takeUntil(this.destroy)
    ).subscribe(a => {
      this.availableAlgorithms = a
      if (this.selected != null && !a.includes(this.selected)) {
        this.selected = null
      }
    })
    this.routeOverviewSelection.selected.pipe(
      takeUntil(this.destroy)
    ).subscribe(s => {
      this.disabled = s.length < 1
    })
  }
  ngOnDestroy(): void {
    this.destroy.next(true)
    this.destroy.complete()
  }

  newSettings(e: number[]) {
    if (this.selected) {
      this.privacyService.currentSetting.next({
        algorithm: this.selected,
        parameters: e
      })
    }
  }

  private initSelected() {
    let currentSettings = this.privacyService.currentSetting.getValue()
    if (currentSettings == null) {
      this.selected = null
    } else {
      this.selected = currentSettings.algorithm
    }
  }

}

