import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { PrivacyAlgorithmMetadata, PrivacyAlgorithmParameter } from 'src/app/models/PrivacyAlgorithmMetadata';
import { PrivacyService } from 'src/app/services/privacy.service';
import { RouteOverviewSelectionService } from 'src/app/services/selection/route-overview-selection.service';

@Component({
  selector: 'app-privacy-algorithm-settings',
  templateUrl: './privacy-algorithm-settings.component.html',
  styleUrls: ['./privacy-algorithm-settings.component.css']
})
export class PrivacyAlgorithmSettingsComponent implements OnChanges, OnDestroy, OnInit {

  @Input() algorithm!: PrivacyAlgorithmMetadata
  @Output() newSettingsEvent = new EventEmitter<number[]>()
  currentSetting: number[] = []
  disabled = true
  destroy = new Subject<boolean>()

  constructor(
    public routeOverviews: RouteOverviewSelectionService,
    public privacyService: PrivacyService
  ) {
    routeOverviews.selected.pipe(
      takeUntil(this.destroy)
    ).subscribe(s => {
      this.disabled = s.length < 1
    })
  }

  ngOnDestroy(): void {
    this.destroy.next(true)
    this.destroy.complete()
  }

  ngOnChanges() {
    this.init()
  }

  ngOnInit(): void {
    this.init()
  }

  // Die Einstellungen werden mit dem Minimum initialisiert,
  // falls noch keine Parameter eingestellt wurden
  init(): void {
    let oldSetting = this.privacyService.currentSetting.getValue()
    if (oldSetting == null) {
      this.currentSetting = this.algorithm.parameters.map(p => p.min)
    } else if (oldSetting != null && oldSetting.algorithm != this.algorithm) {
      this.currentSetting = this.algorithm.parameters.map(p => p.min)
    } else {
      this.currentSetting = oldSetting.parameters
    }
  }

  // Bestimmt, ob der Maximalwert des Parameters kleiner ist, als der Minimalwert.
  isInverted(parameter: PrivacyAlgorithmParameter): boolean {
    return parameter.max < parameter.min
  }

  newSettings(): void {
    // Validiere den input
    for (let i = 0; i < this.currentSetting.length; i++) {
      let parameter: PrivacyAlgorithmParameter = this.algorithm.parameters[i]
      if (this.currentSetting[i] == null) {
        this.currentSetting[i] = parameter.min
      }
      // Falls zu große/zu kleine Werte eingegeben werden, nimm den Maximalwert/Minimalwert des Parameters
      if (!this.isInverted(this.algorithm.parameters[i])) {
        this.currentSetting[i] = Math.max(parameter.min, Math.min(parameter.max, this.currentSetting[i]))
      } else {
        this.currentSetting[i] = Math.min(parameter.min, Math.max(parameter.max, this.currentSetting[i]))
      }
    }
    this.newSettingsEvent.emit(this.currentSetting)
  }

  stepsize(min: number, max: number): number {
    let steps = 100;
    return Math.abs(min - max) / steps
  }

  formatLabel(value: number): number {
    return Math.round(value * 1000) / 1000
  }

}

