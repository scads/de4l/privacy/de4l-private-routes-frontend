import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivacyAlgorithmSettingsComponent } from './privacy-algorithm-settings.component';

describe('PrivacyAlgorithmSettingsComponent', () => {
  let component: PrivacyAlgorithmSettingsComponent;
  let fixture: ComponentFixture<PrivacyAlgorithmSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrivacyAlgorithmSettingsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PrivacyAlgorithmSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
