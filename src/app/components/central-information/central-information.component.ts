import { Component } from '@angular/core';
import { RouteDataService } from 'src/app/services/route-data.service';
import { RouteToMapService } from 'src/app/services/route-to-map.service';
import { MetricService } from 'src/app/services/metric.service';

@Component({
  selector: 'app-central-information',
  templateUrl: './central-information.component.html',
  styleUrls: ['./central-information.component.css']
})
export class CentralInformationComponent {

  constructor(
    public visualizationService: RouteToMapService,
    public routeDataService: RouteDataService,
    public metricService: MetricService
  ) {}

}
