import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { KeycloakService } from 'keycloak-angular';
import { HelpComponent } from '../dialogs/help/help.component';

@Component({
	selector: 'app-titlebar',
	templateUrl: './titlebar.component.html',
	styleUrls: ['./titlebar.component.css']
})
export class TitlebarComponent implements OnInit {

	keycloakConnection: boolean = true;
	title = 'Privacy Tuna';
	user: any = '';

	constructor(
    private keycloakService: KeycloakService,
    private translate: TranslateService,
    private dialog: MatDialog
  ) { }

	ngOnInit(): void {
		this.keycloakService.isLoggedIn().then(resp => {
			this.keycloakConnection = resp.valueOf();
			if (this.keycloakConnection === true) {
				this.keycloakService.loadUserProfile().then(userProfile => this.user = userProfile.username);
			}
		});
	}

	logoutFromDe4l(): void {
		this.keycloakService.logout();
	}

	changeLanguage(language: string): void {
		this.translate.use(language);
	}

  openHelp(){
    this.dialog.open( HelpComponent )
  }
}
