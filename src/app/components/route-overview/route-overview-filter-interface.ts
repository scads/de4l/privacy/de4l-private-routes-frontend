import { EventEmitter, OnDestroy } from '@angular/core';
import { RouteOverview } from 'src/app/models/route-overview';

// Typ für eine Funktion zum Filtern von RouteOverviews. Gibt true zurück,
// wenn die RouteOverview dem Filter entspricht, sonst false
export type RouteOverviewFilter = (ro: RouteOverview) => boolean


export abstract class AbstractRouteOverviewFilterComponent<T> {

  // Enthält die Filterfunktion
  abstract filterFunction(): RouteOverviewFilter

  // Wird emittiert, wenn der Filter verändert wurde
  abstract newFilterEventEmitter: EventEmitter<RouteOverviewFilter>;

  private initValue: T
  private currentValue: T

  public getCurrentValue(): T {
    return this.currentValue
  }

  public setInitValue(i: T) {
    this.initValue = i
  }

  public getInitValue(): T {
    return this.initValue
  }

  constructor(
    initValue: T
  ) {
    this.initValue = initValue
    this.currentValue = initValue
  }

  // Setzt den Filter auf die initialen Werte zurück, sodass nichts gefiltert wird
  public reset(): void {
    this.filterChange(this.initValue)
  };

  // Wird bei Userinput aufgerufen
  public filterChange(newValue: T): void {
    this.currentValue = newValue
    this.emitNewFilter()
  }

  private emitNewFilter(): void {
    this.newFilterEventEmitter.emit(this.filterFunction())
  }

}
