import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { MatSort, MatSortable } from '@angular/material/sort';
import { RouteOverview } from 'src/app/models/route-overview';
import { RouteOverviewSelectionService } from 'src/app/services/selection/route-overview-selection.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatCheckboxDefaultOptions, MAT_CHECKBOX_DEFAULT_OPTIONS } from '@angular/material/checkbox';
import { MatDialog } from '@angular/material/dialog';
import { DeleteRouteDialogComponent } from '../../dialogs/delete-route-dialog/delete-route-dialog.component';
import { DropPointsDialogComponent } from '../../dialogs/drop-points-dialog/drop-points-dialog.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-route-overview-table',
  templateUrl: './route-overview-table.component.html',
  styleUrls: ['./route-overview-table.component.css'],
  providers: [{
    provide: MAT_CHECKBOX_DEFAULT_OPTIONS,
    useValue: { clickAction: 'noop' } as MatCheckboxDefaultOptions
  }]
})
export class RouteOverviewTableComponent implements OnInit, AfterViewInit {

  @Input() data!: Observable<RouteOverview[]>
  dataSource: MatTableDataSource<RouteOverview> = new MatTableDataSource<RouteOverview>([])
  @ViewChild(MatSort) sort!: MatSort;
  numberOfRoutes = 0
  numberOfSelectedRoutes = 0
  numberOfFilteredRoutes = 0

  columnsToDisplay = ['show', 'id', 'name', 'date', 'stepwidth', 'pointCount', 'measurements'].concat(environment.editRoutes ? ['edit'] : [])

  constructor(
    public selection: RouteOverviewSelectionService,
    public dialog: MatDialog,
  ) {
    this.selection.selected.subscribe(s => this.numberOfSelectedRoutes = s.length)
  }

  ngOnInit(): void {
    this.data.subscribe((next) => {
      if (next) {
        this.dataSource.data = next
        this.dataSource.filter = "filter"
        this.numberOfRoutes = next.length
        this.numberOfSelectedRoutes = this.selection.selected.getValue().length
        this.numberOfFilteredRoutes = this.dataSource.filteredData.length
      }
    })
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    setTimeout(() => this.sort.sort(({ id: 'date', start: 'desc' }) as MatSortable), 20)
  }

  applyFilter(filter: (r: RouteOverview) => boolean): void {
    this.dataSource.filterPredicate = filter
    // Retrigger filtering
    this.dataSource.filter = "filter"
    this.numberOfFilteredRoutes = this.dataSource.filteredData.length
  }

  clickOnRoute(routeOverview: RouteOverview, event: MouseEvent) {
    if (event.ctrlKey) {
      this.selection.select(routeOverview)
    } else if (event.shiftKey) {
      let selected: RouteOverview[] = new Array(...this.selection.lastElementsAdded, routeOverview)
      let select = false
      for (let r of this.dataSource.sortData(this.dataSource.data, this.sort)) {
        if (selected.length == 0) {
          break;
        }
        let i = selected.indexOf(r)
        if (i >= 0) {
          select = true
          selected.splice(i, 1)
        }
        if (select) {
          this.selection.add(r)
        }
      }
    } else {
      this.selection.toggle(routeOverview)
    }
  }

  selectAll(): void {
    const numSelected = this.selection.selected.getValue().length
    const numFiltered = this.dataSource.filteredData.length
    if (numFiltered == numSelected) {
      this.selection.select()
    } else {
      // Retrigger filtering
      this.dataSource.filter = "ff"
      this.selection.select(...this.dataSource.filteredData)
    }
  }

  deleteRouteBtnClick(ro: RouteOverview[]) {
    this.dialog.open(DeleteRouteDialogComponent, { data: ro })
    // Retrigger filtering
    this.dataSource.filter = "ff"
  }

  dropPointsBtnClick(ro: RouteOverview[]) {
    this.dialog.open(DropPointsDialogComponent, { data: ro })
    // Retrigger filtering
    this.dataSource.filter = "ff"
  }

}
