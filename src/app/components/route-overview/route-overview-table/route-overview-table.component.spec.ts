import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteOverviewTableComponent } from './route-overview-table.component';

describe('RouteOverviewTableComponent', () => {
  let component: RouteOverviewTableComponent;
  let fixture: ComponentFixture<RouteOverviewTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RouteOverviewTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteOverviewTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
