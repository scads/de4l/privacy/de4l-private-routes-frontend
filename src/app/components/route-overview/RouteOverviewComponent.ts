import { Component, OnDestroy, ViewChild } from '@angular/core';
import { Observable, Subject, takeUntil } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { RouteOverview } from 'src/app/models/route-overview';
import { RouteOverviewService as RouteOverviewService } from 'src/app/services/route-overview.service';
import { RouteOverviewSelectionService } from 'src/app/services/selection/route-overview-selection.service';
import { VisibilityService } from 'src/app/services/visibility.service';
import { RouteOverviewTableComponent } from './route-overview-table/route-overview-table.component';
import { PointDisplayWarningComponent } from 'src/app/components/dialogs/point-display-warning/point-display-warning.component';


@Component({
  selector: 'app-route-overview',
  templateUrl: './route-overview.component.html',
  styleUrls: ['./route-overview.component.css']
})
export class RouteOverviewComponent implements OnDestroy {

  routesOverviewDataSource: Observable<RouteOverview[]>;
  visibilityOfFilters: boolean = false;
  @ViewChild(RouteOverviewTableComponent) table!: RouteOverviewTableComponent;
  destroy = new Subject<boolean>()

  constructor(
    public overviewService: RouteOverviewService,
    public visibility: VisibilityService,
    public selection: RouteOverviewSelectionService,
    private dialog: MatDialog
  ) {
    this.routesOverviewDataSource = overviewService.routes;
    this.routesOverviewDataSource.pipe(
      takeUntil(this.destroy)
    ).subscribe(s => {
      if (s) {
        this.visibilityOfFilters = s.length > 0;
      }
    });
  }

  ngOnDestroy(): void {
    this.destroy.next(true)
    this.destroy.complete()
  }

  updateTableContent(): void {
    this.overviewService.updateRouteOverviews();
  }

  displayRoutesAndCloseDrawer(): void {
    if (this.selection.currentlySelectedPoints() > 500) {
      this.dialog.open(PointDisplayWarningComponent, {});
    }
    else {
      this.visibility.drawerOpen = false;
      this.overviewService.displayedRoutes.next(this.selection.selected.getValue());
    }
  }

}

