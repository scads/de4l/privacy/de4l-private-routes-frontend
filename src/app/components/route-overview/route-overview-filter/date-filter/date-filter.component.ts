import { Component, EventEmitter, OnDestroy, Output } from '@angular/core';
import { RouteOverviewService } from 'src/app/services/route-overview.service';
import { AbstractRouteOverviewFilterComponent, RouteOverviewFilter } from '../../route-overview-filter-interface';

@Component({
  selector: 'app-date-filter',
  templateUrl: './date-filter.component.html',
  styleUrls: ['./date-filter.component.css']
})
export class DateFilterComponent extends AbstractRouteOverviewFilterComponent<Date[]> {

  @Output() newFilterEventEmitter = new EventEmitter<RouteOverviewFilter>();

  constructor(
    ros: RouteOverviewService
  ) {
    super([])

    // Falls sich der Datensatz ändert, setze die initValue neu
    ros.minValues.subscribe((s) => { this.setInitValue([s.date, this.getInitValue()[1]]) })
    ros.maxValues.subscribe((s) => { this.setInitValue([this.getInitValue()[0], s.date]) })
  }

  filterFunction(): RouteOverviewFilter {
    return (r) => {
      if (this.getCurrentValue()[0] && this.getCurrentValue()[1]) {
        return r.date <= this.getCurrentValue()[1] && r.date >= this.getCurrentValue()[0];
      } else if (this.getCurrentValue()[0]) {
        return r.date >= this.getCurrentValue()[0]
      } else if (this.getCurrentValue()[1]) {
        return r.date <= this.getCurrentValue()[1]
      } else {
        return true
      }
    }
  }

}
