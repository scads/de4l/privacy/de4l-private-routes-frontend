import { Component, EventEmitter, Output } from '@angular/core';
import { RouteOverviewService as RouteOverviewService } from 'src/app/services/route-overview.service';
import { RouteOverviewSelectionService } from 'src/app/services/selection/route-overview-selection.service';
import { AbstractRouteOverviewFilterComponent, RouteOverviewFilter } from '../../route-overview-filter-interface';

@Component({
  selector: 'app-selected-filter',
  templateUrl: './selected-filter.component.html',
  styleUrls: ['./selected-filter.component.css']
})
export class SelectedFilterComponent extends AbstractRouteOverviewFilterComponent<boolean | null> {

  @Output() newFilterEventEmitter = new EventEmitter<RouteOverviewFilter>();

  constructor(
    private selection: RouteOverviewSelectionService,
    ros: RouteOverviewService,
  ) {
    super(null);
    // Falls sich der Datensatz ändert, setze die initValue neu
    ros.maxValues.subscribe(() => { this.setInitValue(null) })
  }

  filterFunction(): RouteOverviewFilter {
    return (r) => {
      if (this.getCurrentValue() != null) {
        if (this.getCurrentValue()) {
          return this.selection.isSelected(r)
        } else {
          return !this.selection.isSelected(r)
        }
      } else {
        return true
      }
    }
  }

}
