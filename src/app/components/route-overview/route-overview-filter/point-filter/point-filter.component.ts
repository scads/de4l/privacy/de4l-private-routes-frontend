import { Component, EventEmitter, Output } from '@angular/core';
import { RouteOverviewService } from 'src/app/services/route-overview.service';
import { AbstractRouteOverviewFilterComponent, RouteOverviewFilter } from '../../route-overview-filter-interface';

@Component({
  selector: 'app-point-filter',
  templateUrl: './point-filter.component.html',
  styleUrls: ['./point-filter.component.css']
})
export class PointFilterComponent extends AbstractRouteOverviewFilterComponent<number[]> {

  @Output() newFilterEventEmitter = new EventEmitter<RouteOverviewFilter>();

  filterFunction(): RouteOverviewFilter {
    return (r) => {
      if (this.getCurrentValue()[0] && this.getCurrentValue()[1]) {
        return r.pointCount <= this.getCurrentValue()[1] && r.pointCount >= this.getCurrentValue()[0];
      } else if (this.getCurrentValue()[0]) {
        return r.pointCount >= this.getCurrentValue()[0]
      } else if (this.getCurrentValue()[1]) {
        return r.pointCount <= this.getCurrentValue()[1]
      } else {
        return true
      }
    }
  }

  constructor(
    ros: RouteOverviewService
  ) {
    super([0, 1000]);
    // Falls sich der Datensatz ändert, setze die initValue neu
    ros.minValues.subscribe((s) => { this.setInitValue([s.pointCount, this.getInitValue()[1]]) })
    ros.maxValues.subscribe((s) => { this.setInitValue([this.getInitValue()[0], s.pointCount]) })
  }

}
