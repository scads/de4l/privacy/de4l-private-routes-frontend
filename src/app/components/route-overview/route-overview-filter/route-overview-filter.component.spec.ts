import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteOverviewFilterComponent } from './route-overview-filter.component';

describe('RouteOverviewFilterComponent', () => {
  let component: RouteOverviewFilterComponent;
  let fixture: ComponentFixture<RouteOverviewFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RouteOverviewFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteOverviewFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
