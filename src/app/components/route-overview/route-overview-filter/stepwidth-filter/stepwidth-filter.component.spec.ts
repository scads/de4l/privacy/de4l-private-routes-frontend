import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StepwidthFilterComponent } from './stepwidth-filter.component';

describe('StepwidthFilterComponent', () => {
  let component: StepwidthFilterComponent;
  let fixture: ComponentFixture<StepwidthFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StepwidthFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StepwidthFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
