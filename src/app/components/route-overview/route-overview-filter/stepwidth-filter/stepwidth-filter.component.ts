import { Component, EventEmitter, Output } from '@angular/core';
import { RouteOverviewService } from 'src/app/services/route-overview.service';
import { AbstractRouteOverviewFilterComponent, RouteOverviewFilter } from '../../route-overview-filter-interface';

@Component({
  selector: 'app-stepwidth-filter',
  templateUrl: './stepwidth-filter.component.html',
  styleUrls: ['./stepwidth-filter.component.css']
})
export class StepwidthFilterComponent extends AbstractRouteOverviewFilterComponent<number[]> {

  @Output() newFilterEventEmitter = new EventEmitter<RouteOverviewFilter>();

  filterFunction(): RouteOverviewFilter {
    return (r) => {
      if (this.getCurrentValue()[0] && this.getCurrentValue()[1]) {
        return r.stepwidth <= this.getCurrentValue()[1] && r.stepwidth >= this.getCurrentValue()[0];
      } else if (this.getCurrentValue()[0]) {
        return r.stepwidth >= this.getCurrentValue()[0]
      } else if (this.getCurrentValue()[1]) {
        return r.stepwidth <= this.getCurrentValue()[1]
      } else {
        return true
      }
    }
  }

  constructor(
    ros: RouteOverviewService
  ) {
    super([0, 100000]);
    // Falls sich der Datensatz ändert, setze die initValue neu
    ros.minValues.subscribe((s) => {
      this.setInitValue([
        Math.floor(s.stepwidth),
        this.getInitValue()[1]])
    })
    ros.maxValues.subscribe((s) => {
      this.setInitValue([
        this.getInitValue()[0],
        Math.ceil(s.stepwidth)])
    })
  }

}
