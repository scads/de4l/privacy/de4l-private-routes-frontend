import { Component, EventEmitter, Output } from '@angular/core';
import { RouteOverviewService } from 'src/app/services/route-overview.service';
import { AbstractRouteOverviewFilterComponent, RouteOverviewFilter } from '../../route-overview-filter-interface';

@Component({
  selector: 'app-name-filter',
  templateUrl: './name-filter.component.html',
  styleUrls: ['./name-filter.component.css']
})
export class NameFilterComponent extends AbstractRouteOverviewFilterComponent<string> {

  @Output() newFilterEventEmitter = new EventEmitter<RouteOverviewFilter>();

  filterFunction(): RouteOverviewFilter {
    return (r) => {
      if (this.getCurrentValue()) {
        return r.name.toLowerCase().includes(this.getCurrentValue().toLowerCase())
      } else {
        return true;
      }
    }
  }

  constructor(
    ros: RouteOverviewService
  ) {
    super("");
    // Falls sich der Datensatz ändert, setze die initValue neu
    ros.maxValues.subscribe(() => { this.setInitValue("") })
  }

}
