import { AfterViewInit, Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { RouteOverview } from 'src/app/models/route-overview';
import { AbstractRouteOverviewFilterComponent, RouteOverviewFilter } from '../route-overview-filter-interface';

@Component({
  selector: 'app-route-overview-filter',
  templateUrl: './route-overview-filter.component.html',
  styleUrls: ['./route-overview-filter.component.css']
})
export class RouteOverviewFilterComponent implements AfterViewInit {

  filters: RouteOverviewFilter[] = []

  @Output() newFilter = new EventEmitter<RouteOverviewFilter>()

  @ViewChild('id') id!: AbstractRouteOverviewFilterComponent<number[]>
  @ViewChild('point') point!: AbstractRouteOverviewFilterComponent<number[]>
  @ViewChild('stepwidth') stepwidth!: AbstractRouteOverviewFilterComponent<number[]>
  @ViewChild('name') name!: AbstractRouteOverviewFilterComponent<string>
  @ViewChild('selection') selection!: AbstractRouteOverviewFilterComponent<boolean | null>
  @ViewChild('date') date!: AbstractRouteOverviewFilterComponent<Date[]>

  constructor() { }

  ngAfterViewInit(): void {
    this.filters.push(this.id.filterFunction())
    this.filters.push(this.point.filterFunction())
    this.filters.push(this.stepwidth.filterFunction())
    this.filters.push(this.name.filterFunction())
    this.filters.push(this.selection.filterFunction())
    this.filters.push(this.date.filterFunction())
  }

  emitNewFilter(): void {
    this.newFilter.emit(this.filters.reduce((a, b) => {
      return (ro: RouteOverview) => a(ro) && b(ro)
    }))
  }

  resetAllFilters(): void {
    this.name.reset()
    this.id.reset()
    this.point.reset()
    this.stepwidth.reset()
    this.date.reset()
    this.selection.reset()
  }

}
