import { OnInit, Component, ViewChild, } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { MapPositionService } from 'src/app/services/map-position.service';
import { VisibilityService } from 'src/app/services/visibility.service';
import { ImportDataComponent } from 'src/app/components/dialogs/import-data/import-data.component';
import { ExportDataComponent } from 'src/app/components/dialogs/export-data/export-data.component';
import { MatMenuTrigger } from '@angular/material/menu';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'app-menu',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.css']
})
export class MenuComponent {

	@ViewChild(MatMenuTrigger) trigger!: MatMenuTrigger;

	constructor(
		private mapPosition: MapPositionService,
		private translate: TranslateService,
		private visibility: VisibilityService,
		public dialog: MatDialog,
	) { }

  environment()  {
    return environment
  }

	importData(): void {
		this.dialog.open(ImportDataComponent, {
  		panelClass: 'app-full-bleed-dialog' 
		});
	}

	exportData(): void {
		this.dialog.open(ExportDataComponent);
	}

	rightFullClick(){
		if ( (!this.visibility.visibilityOfRightMap)  && this.visibility.visibilityOfLeftMap ) {
			this.visibility.normalMapSizes()
		} else {
			this.visibility.rightMapFullSize()
		}
	}

	rightClickDisabled(): boolean {
		return !this.visibility.visibilityOfLeftMap && this.visibility.visibilityOfRightMap
	}

	leftFullClick(){
		if ( (!this.visibility.visibilityOfLeftMap)  && this.visibility.visibilityOfRightMap ) {
			this.visibility.normalMapSizes()
		} else {
			this.visibility.leftMapFullSize()
		}
	}

	leftClickDisabled(): boolean {
		return !this.visibility.visibilityOfRightMap && this.visibility.visibilityOfLeftMap
	}

	toggleSync(){
		this.mapPosition.toggleSync()
	}

	getSyncState():string {
		return !this.mapPosition.syncMaps ? this.translate.instant('On') : this.translate.instant('Off') 
	}

	openDatabaseOverview(): void {
		this.visibility.drawerOpen = true
	}
}
