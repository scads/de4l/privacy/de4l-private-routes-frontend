import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-map-header',
  templateUrl: './map-header.component.html',
  styleUrls: ['./map-header.component.css']
})
export class MapHeaderComponent {

	@Input() title: String = "Title"

  constructor() { }

}
