import { Component, Input, OnInit } from '@angular/core';
import { tileLayer, Map, LatLng, Layer } from 'leaflet';
import { BehaviorSubject, Subject } from 'rxjs';
import { LayersControl } from 'src/app/models/layers-control';
import { BoundingBoxService } from 'src/app/services/bounding-box.service';
import { MapIds } from 'src/app/services/definitions';
import { MapPositionService } from 'src/app/services/map-position.service';
import { MapService } from 'src/app/services/map.service';

@Component({
  selector: 'app-leaflet-map',
  templateUrl: './leaflet-map.component.html',
  styleUrls: ['./leaflet-map.component.css'],
})
export class LeafletMapComponent implements OnInit {

  @Input() id!: MapIds
  @Input() title: string = "TITLE"
  @Input() center!: BehaviorSubject<LatLng>
  @Input() zoom!: BehaviorSubject<number>
  @Input() layers: Layer[] = []
  @Input() layersControl = new LayersControl()

  map?: Map;

  mapInitOptions = {
    layers: [
      tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibXNjaG5laWRlcmluZm9ybWF0aWsiLCJhIjoiY2tqaWp2bGJzMWNmYTMxbnZrMmNtN3R1cyJ9.gcbGIruQ9cffL5fSaZ8Xjg',
        {
          id: 'mapbox/streets-v11',
          tileSize: 512,
          zoomOffset: -1,
          accessToken: 'your.mapbox.access.token',
          attribution: '...'
        }
      )
    ],

   //zoom: this.mapPositionService.defaultZoom,
    //center: this.mapPositionService.defaultCenter

  };

  constructor(
    public mapService: MapService,
    public mapPositionService: MapPositionService,
    public boundingBoxService: BoundingBoxService
  ) { 
  }
  ngOnInit(): void {
    this.map?.invalidateSize()
  }

  // Saves the reference to the map in the map service.
  onMapReady(map: Map) {
    this.map = map;
    this.mapService.addMap(this.id, this.map);
  }

  mapMoveEnd(event: any) {
    if (this.map) {
      this.boundingBoxService.updateBoundingBox(this.id, this.map!.getBounds())
    }
  }

}
