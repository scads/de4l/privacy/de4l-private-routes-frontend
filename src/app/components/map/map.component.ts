import { Component, Input } from '@angular/core';
import { LatLng, Layer } from 'leaflet';
import { BehaviorSubject, map, Subject } from 'rxjs';
import { LayersControl } from 'src/app/models/layers-control';
import { Route } from 'src/app/models/route';
import { MapIds } from 'src/app/services/definitions';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent {

  @Input() title!: string
  @Input() id!: MapIds
  @Input() routes: Route[] = []
  @Input() layers!: Layer[]
  @Input() layersControl!: LayersControl
  @Input() center!: BehaviorSubject<LatLng>
  @Input() zoom!: BehaviorSubject<number>

  constructor() {
  }

}
