import { Component, Input } from '@angular/core';
import { MetricService } from 'src/app/services/metric.service';
import { MetricMetadata } from "src/app/models/metric-metadata";
import { RoutesWithMetrics } from "src/app/models/routes-with-metrics";

@Component({
    selector: 'app-metric-viewer',
    templateUrl: './metric-viewer.component.html',
    styleUrls: ['./metric-viewer.component.css']
})
export class MetricViewerComponent {

    @Input() data: RoutesWithMetrics | null = null
    hoveredTile: number = -1;

    constructor(
        public metricService: MetricService,
    ) {
    }

    onClickMetric(m: MetricMetadata) {
        this.metricService.basisForVisualization = m
        this.metricService.metricForVisualizationSubject.next(m)
    }
}

