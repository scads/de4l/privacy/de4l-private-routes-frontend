import { Component } from '@angular/core';
import { ColoringService } from 'src/app/services/coloring.service';
import { MapPositionService } from 'src/app/services/map-position.service';

@Component({
  selector: 'app-private-routes-footer',
  templateUrl: './private-routes-footer.component.html',
  styleUrls: ['./private-routes-footer.component.css']
})
export class PrivateRoutesFooterComponent {

  constructor(private coloring: ColoringService, public positionService: MapPositionService ) { }

  ngOnInit(): void {
  }

  createPrivacyColorSpectrum(input: number): Object {
    let color = this.coloring.calculateColorByMetric(input);
    return {background: color, color: color};
  }

  linearGradient(): string {
    return 'background-image: linear-gradient(to right,' + this.coloring.calculateColorByMetric(0)+ 
      ", " + this.coloring.calculateColorByMetric(100)+ ");"
  }


}
