import {Component, OnInit} from '@angular/core';
import {KeycloakService} from "keycloak-angular";

@Component({
  selector: 'app-page-not-found',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.css']
})
export class PublicComponent {

  constructor(private keycloakService: KeycloakService) {
  }

  routeToLoginMask() {
    this.keycloakService.login();
  }
}
