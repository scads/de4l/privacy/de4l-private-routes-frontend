import { Component } from '@angular/core';
import { MapIds } from 'src/app/services/definitions';
import { MapPositionService } from 'src/app/services/map-position.service';
import { MapService } from 'src/app/services/map.service';
import { TranslateService } from '@ngx-translate/core';
import { RouteDataService } from 'src/app/services/route-data.service';
import { VisibilityService } from 'src/app/services/visibility.service';
import { RouteToMapService } from 'src/app/services/route-to-map.service';

@Component({
	selector: 'app-private-routes',
	templateUrl: './private-routes.component.html',
	styleUrls: ['./private-routes.component.css']
})
export class PrivateRoutesComponent {
	mapIds = MapIds;

	constructor(
		public mapService: MapService,
		public visibility: VisibilityService,
		public position: MapPositionService,
		public routeDataService: RouteDataService,
		public routeToMapService: RouteToMapService,
		public translate: TranslateService,
	) { }

}
