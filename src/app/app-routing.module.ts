import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './views/page-not-found/page-not-found.component';
import {AuthGuard} from "./services/guard.service";
import { PrivateRoutesComponent } from './views/private-routes/private-routes.component';
import { environment } from 'src/environments/environment';

const routes: Routes = [
  {
    path: '',
    component: PrivateRoutesComponent,
    canActivate: environment.keycloak ? [AuthGuard] : []
   // canActivate: [AuthGuard]
  },
  {
		path: '**', 
		component: PageNotFoundComponent
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
