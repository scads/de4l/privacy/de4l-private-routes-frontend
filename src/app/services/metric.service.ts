import { Injectable } from '@angular/core';
import { RoutesWithMetrics } from "../models/routes-with-metrics";
import { MetricMetadata } from "../models/metric-metadata";
import { Subject } from 'rxjs';
import { RouteDataService } from 'src/app/services/route-data.service';
import { ColoringService } from 'src/app/services/coloring.service';
import { CommunicationService } from 'src/app/services/communication.service';
import { RouteSelectionService } from './selection/route-selection.service';
import { MetricPerformanceMeasures } from "../models/metric-performance-measures";

@Injectable({
	providedIn: 'root'
})
export class MetricService {

	availablePrivacyMetrics: MetricMetadata[] = []
	availableUtilityMetrics: MetricMetadata[] = []
	currentPrivacyMetrics: MetricMetadata[] = []
    currentUtilityMetrics: MetricMetadata[] = []
    selectedMetrics: MetricMetadata[] = []
    selectedMetricsSubject = new Subject<MetricMetadata[]>()
	basisForVisualization!: MetricMetadata
	metricForVisualizationSubject = new Subject<MetricMetadata>()

	constructor(
		public routeDataService: RouteDataService,
		private coloring: ColoringService,
		private communication: CommunicationService,
		public selection: RouteSelectionService,
	) {
        this.routeDataService.publicRoutesSubject.subscribe((r) => {
            if (r != null && r.routes.length > 0) {
                let metricIds = r.metrics.map(m => m.id)
                this.routeDataService.currentlyFetchedMetrics = this.availablePrivacyMetrics.concat(this.availableUtilityMetrics).filter(m => metricIds.includes(m.id))
            }
        })

        this.routeDataService.privateRoutesSubject.subscribe((r) => {
            if (r != null && r.routes.length > 0) {
                let metricIds = r.metrics.map(m => m.id)
                this.routeDataService.currentlyFetchedMetrics = this.availablePrivacyMetrics.concat(this.availableUtilityMetrics).filter(m => metricIds.includes(m.id))
            }
        })
	}

	public fetchAvailableMetrics() {
	    this.communication.getAvailableMetrics().subscribe( (metrics) => {
	        this.availablePrivacyMetrics = metrics.filter((m) => m.type === 'privacy')
	        this.availableUtilityMetrics = metrics.filter((m) => m.type === 'utility')
	        this.currentPrivacyMetrics = [this.availablePrivacyMetrics[0]]
	        this.currentUtilityMetrics = [this.availableUtilityMetrics[0]]
	        this.selectedMetrics = this.currentPrivacyMetrics.concat(this.currentUtilityMetrics);
	        this.basisForVisualization = this.selectedMetrics[0];
	        this.metricForVisualizationSubject.next(this.basisForVisualization)
	    })
	}

	getMetricGlobal(metric_id: number, data: RoutesWithMetrics | null) {
	    if ( data != null ) {
            let value: number[] | undefined = data.metrics.filter(m => m.id == metric_id).map(m => {return m.value})
            return (value == undefined) ? 0 : Math.round((value[0] + Number.EPSILON)*100)/100;
        }
        return 0
	}

	getMetricPerformance(metric_id: number, data: RoutesWithMetrics | null, type: string) {
        if ( data != null ) {
            const metric = data.metrics.find(m => m.id === metric_id);
            if (metric) {
                const value: number | undefined = metric.performance?.[type as keyof MetricPerformanceMeasures];
                return (value === undefined) ? 0 : Math.round((value * 100 + Number.EPSILON) * 100) / 100;
            }
        }
        return 0
    }

	onPrivacyChange(selectedPrivacy: MetricMetadata[] | null) {
        if (selectedPrivacy != null) {
            this.currentPrivacyMetrics = selectedPrivacy;
            this.selectedMetrics = this.currentPrivacyMetrics.concat(this.currentUtilityMetrics)
            this.selectedMetricsSubject.next(this.selectedMetrics);
            if (!this.selectedMetrics.includes(this.basisForVisualization)) {
                this.basisForVisualization = this.selectedMetrics[0]
                this.metricForVisualizationSubject.next(this.basisForVisualization)
            }
        }
	}

    onUtilityChange(selectedUtility: MetricMetadata[] | null) {
        if (selectedUtility != null) {
            this.currentUtilityMetrics = selectedUtility;
            this.selectedMetrics = this.currentPrivacyMetrics.concat(this.currentUtilityMetrics)
            this.selectedMetricsSubject.next(this.selectedMetrics);
            if (!this.selectedMetrics.includes(this.basisForVisualization)) {
                this.basisForVisualization = this.selectedMetrics[0]
                this.metricForVisualizationSubject.next(this.basisForVisualization)
            }
        }
    }

    onVisualizationMetricChange() {
        this.metricForVisualizationSubject.next(this.basisForVisualization);
    }

	public createMetricColorSpectrum(input: number): Object {
		return {'background': this.coloring.calculateColorByMetric(input), 'width': input + '%'};
	}

}
