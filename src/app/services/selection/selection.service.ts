import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export abstract class SelectionService<T> {

  selected: BehaviorSubject<T[]> = new BehaviorSubject<T[]>([])

  // Holds the elements, that were added in last modification of the selection
  lastElementsAdded: T[] = []

  // Function to test the item for equality. This function is used while checking,
  // if an item is selected
  equalityfn = (itemA: T, itemB: T) => itemA == itemB

  name = "Abstract SelectionService";

  constructor() {
  }

  private findIndex(itemA: T, collection: T[] = this.selected.getValue()) {
    return collection.findIndex((itemB) => this.equalityfn(itemA, itemB))
  }

  public isSelected(itemA: T) {
    return this.findIndex(itemA) > -1
  }

  // Select items
  public select(...items: T[]) {
    if (!items) { items = [] }
    this.lastElementsAdded = items
    this.selected.next(items)
  }

  // If item is the only one selected, select nothing, else select item
  public toggleSelect(item: T) {
    if (item) {
      if (this.selected.getValue().length === 1 && this.isSelected(item)) {
        this.selected.next([])
        this.lastElementsAdded = [];
      } else {
        this.lastElementsAdded = [item];
        this.selected.next([item])
      }
    }
  }

  // Add items to selection
  public add(...items: T[]) {
    if (!items) { return }
    if (items.length == 0) { return }
    this.lastElementsAdded = items
    let newValues = this.selected.getValue();
    items.forEach(i => {
      if (!this.isSelected(i)) {
        newValues.push(i)
      }
    })
    this.selected.next(newValues)
  }

  // Toggle selection of every item
  public toggle(...items: T[]) {
    if (!items) { items = [] }
    if (items.length == 0) { return }
    this.lastElementsAdded = []
    let newValues = this.selected.getValue();
    items.forEach((item: T) => {
      let i = this.findIndex(item)
      if (i > -1) {
        newValues.splice(i, 1)
      } else {
        newValues.push(item)
        this.lastElementsAdded.push(item)
      }
    })
    this.selected.next(newValues)
  }

  // Remove all items from selection
  public remove(...items: T[]) {
    if (!items) { return }
    if (items.length == 0) { return }
    let changed = false
    let newValue = this.selected.getValue()
    items.forEach((item) => {
      const index = this.findIndex(item, newValue)
      if (index > -1) {
        newValue.splice(index, 1);
        changed = true
      }
    })
    if (changed) {
      this.selected.next(newValue)
    }
  }

  // Remove every item from selection, that is not in items
  public prune(...items: T[]) {
    if (!items) { return }
    let changed = false
    let newValue = this.selected.getValue()
    newValue = newValue.filter((item) => {
      if (this.findIndex(item, items) == -1) {
        changed = true
        return false
      }
      return true
    })
    if (changed) {
      this.selected.next(newValue)
    }
  }

}
