import { Injectable } from '@angular/core';
import { RouteOverview } from 'src/app/models/route-overview';
import { RouteOverviewService as RouteOverviewService } from '../route-overview.service';
import { SelectionService } from './selection.service';

@Injectable({
  providedIn: 'root'
})
export class RouteOverviewSelectionService extends SelectionService<RouteOverview> {

  constructor(
    databaseOverviewService: RouteOverviewService,
  ) {
    super()
    this.name = "RouteOverviewSelectionService"
    this.equalityfn = (a, b) => a.id == b.id && a.name == b.name

    // If RouteOverviews changed,
    // remove gone RouteOverviews from selection
    databaseOverviewService.routes.subscribe((ros) => {
      this.prune(...ros)
    })
  }

  public currentlySelectedIds(): number[] {
    return this.selected.getValue().map((r) => {
      return r.id
    })
  }

  public currentlySelectedPoints(): number {
    return this.selected.getValue().map((r) => {
      return r.pointCount
    }).reduce((sum, num) => sum + num, 0)
  }

}
