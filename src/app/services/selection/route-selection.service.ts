import { Injectable } from '@angular/core';
import { Route } from 'src/app/models/route';
import { SelectionService } from './selection.service';

@Injectable({
  providedIn: 'root'
})
export class RouteSelectionService extends SelectionService<Route> {

  constructor() { 
		super()
		this.name = "RouteSelectionService"
	}
}
