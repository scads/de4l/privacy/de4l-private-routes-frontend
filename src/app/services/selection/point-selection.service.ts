import { Injectable } from '@angular/core';
import { Geodata } from 'src/app/models/geodata';
import { SelectionService } from './selection.service';

@Injectable({
  providedIn: 'root'
})
export class PointSelectionService extends SelectionService<Geodata>{

  constructor() { 
		super()
	}
}
