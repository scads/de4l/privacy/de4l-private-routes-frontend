import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { RouteOverview } from '../models/route-overview';
import { CommunicationService } from './communication.service';

@Injectable({
  providedIn: 'root'
})
export class RouteOverviewService {

  routes: BehaviorSubject<RouteOverview[]>
  displayedRoutes = new BehaviorSubject<RouteOverview[]>([])

  // Holds constructed RouteOverviews, that represent the min and max values of the dataset.
  minValues: Observable<RouteOverview>
  maxValues: Observable<RouteOverview>

  constructor(
    private communication: CommunicationService,
  ) {
    this.routes = new BehaviorSubject<RouteOverview[]>([]);
    this.maxValues = new Observable<RouteOverview>((o) => {
      this.routes.subscribe((ros) => { if (ros.length > 0) o.next(this.calculateMaxRouteOverview(ros)) })
    })
    this.minValues = new Observable<RouteOverview>((o) => {
      this.routes.subscribe((ros) => { if (ros.length > 0) o.next(this.calculateMinRouteOverview(ros)) })
    })
  }

  updateRouteOverviews(): void {
    this.communication.getOverview().subscribe(x => { this.routes.next(x) })
  }

  getRoutes(): RouteOverview[] {
    return this.routes.getValue()
  }

  private calculateMaxRouteOverview(ros: RouteOverview[]): RouteOverview {
    let maxRo = { ...ros[0] }
    ros.forEach((r) => {
      if (r.date > maxRo.date) maxRo.date = r.date;
      if (r.stepwidth > maxRo.stepwidth) maxRo.stepwidth = r.stepwidth;
      if (r.pointCount > maxRo.pointCount) maxRo.pointCount = r.pointCount;
      if (r.id > maxRo.id) maxRo.id = r.id;
    })
    return maxRo
  }

  private calculateMinRouteOverview(ros: RouteOverview[]): RouteOverview {
    let minRo = { ...ros[0] }
    ros.forEach((r) => {
      if (r.date < minRo.date) minRo.date = r.date;
      if (r.stepwidth < minRo.stepwidth) minRo.stepwidth = r.stepwidth;
      if (r.pointCount < minRo.pointCount) minRo.pointCount = r.pointCount;
      if (r.id < minRo.id) minRo.id = r.id;
    })
    return minRo
  }

}
