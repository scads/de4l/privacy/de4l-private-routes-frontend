import { Injectable } from '@angular/core';
import {LatLngBounds } from 'leaflet';
import { debounceTime, Subject } from 'rxjs';
import { MapIds } from './definitions';

@Injectable({
  providedIn: 'root'
})
export class BoundingBoxService {

  syncMaps = true
  controller = MapIds.rightMap

  private boundingBoxes: { [key: string]: Subject<LatLngBounds> } = {}

  constructor(
  ) {
    this.boundingBoxes[MapIds.leftMap.valueOf()] = new Subject<LatLngBounds>
    this.boundingBoxes[MapIds.rightMap.valueOf()] = new Subject<LatLngBounds>
    //this.getBoundingBox(MapIds.rightMap).subscribe(v => { console.log("rightmap", v) })
    //this.getBoundingBox(MapIds.leftMap).subscribe(v => console.log("leftmap", v))

  }
  private getBoundingBoxSubject(mapId: MapIds) {
    return this.boundingBoxes[mapId.valueOf()]
  }

  getBoundingBox(mapId: MapIds) {
    return this.getBoundingBoxSubject(mapId).asObservable().pipe(debounceTime(0.5))
  }

  updateBoundingBox(mapId: MapIds, newBounds: LatLngBounds) {
    if (!this.getBoundingBoxSubject(mapId)) {
      throw new Error("Map not registered!")
    }
    this.getBoundingBoxSubject(mapId).next(newBounds)
  }

}
