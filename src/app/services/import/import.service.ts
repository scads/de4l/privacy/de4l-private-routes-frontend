import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { CommunicationService } from '../communication.service';

@Injectable({
	providedIn: 'root'
})
export abstract class ImportService<T> {

	// Der Name des Dateiformats
	public abstract name: String

	// Eine Beschreibung des Dateiformats
	public abstract desc: String

	// Name des Enpoints im SpringBoot, zum posten der Daten
	public abstract endpoint: string

	// Maximale Dateigröße (10mb)
	public maxFileSize: number = 10 * 1024 * 1024

	// Der Filetype der Datei (.json, .csv, ...)
	public abstract fileType: string

	constructor(
		public communication: CommunicationService,
		public bar: MatSnackBar,
	) {
	}

	public abstract parseFile( file: File ): Observable<T[]>

	public importData(data: T[]) {
		return this.communication.importData( data, this.endpoint )
	}

	public handleError( message: string ) {
		this.bar.open("There was an error: \n" + message )
	}

}
