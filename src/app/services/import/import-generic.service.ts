import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { GenericRouteDto } from 'src/app/models/GenericRouteDto';
import { CommunicationService } from '../communication.service';
import { ImportService } from './import.service';
import { ImportFeedbackComponent } from 'src/app/components/dialogs/import-feedback/import-feedback.component';

@Injectable({
  providedIn: 'root'
})
export class ImportGenericService extends ImportService<GenericRouteDto> {

  public endpoint: string = "generic"
  public name: String = this.translate.instant("ImportGenericName")
  public desc: String = this.translate.instant("ImportGenericDesc")
  public fileType: string = ".json"

  constructor(
    override communication: CommunicationService,
    override bar: MatSnackBar,
    private translate: TranslateService,
    public dialog: MatDialog,
  ) {
    super(communication, bar);
    this.translate.onLangChange.subscribe(() => {
      this.name = this.translate.instant("ImportGenericName")
      this.desc = this.translate.instant("ImportGenericDesc")
    });
  }

  public parseFile(file: File): Observable<GenericRouteDto[]> {
    const fileReader = new FileReader()
    fileReader.readAsText(file)
    return new Observable( (observer) => {
      fileReader.onload = () => { 
        let result = ""
        if ( typeof fileReader.result == 'string' ) {
          result = fileReader.result
        }
        let json: GenericRouteDto[] = []
        try {
         json = JSON.parse( result )
        } catch (e) {
          this.communication.inputError = String(e)
          this.dialog.open(ImportFeedbackComponent, {
              data: {
                filename: file.name,
                responseArray: []
              }
          });
          console.log("ERROR:", String(e));
          return
        }
        observer.next( json )
      }
      fileReader.onerror = (e) => { 
        this.dialog.open(ImportFeedbackComponent, {
            data: {
              filename: file.name,
              responseArray: []
            }
        });
        this.communication.inputError = String(e)
        console.log("ERROR:", String(e));
      }
    })
  }

}
