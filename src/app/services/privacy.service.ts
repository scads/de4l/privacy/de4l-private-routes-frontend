import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PrivacyAlgorithmMetadata } from '../models/PrivacyAlgorithmMetadata';
import { PrivacyAlgorithmSetting } from '../models/PrivacyAlgorithmSetting';
import { CommunicationService } from './communication.service';

@Injectable({
	providedIn: 'root'
})
export class PrivacyService {

	public currentSetting = new BehaviorSubject<PrivacyAlgorithmSetting | null>(null)
  public availableAlgorithms = new BehaviorSubject<PrivacyAlgorithmMetadata[]>([])

	constructor(
    public communication: CommunicationService
	) { }

  public fetchAvailableAlgorithms() {
    this.communication.getAvailableAlgorithms().subscribe( (s) => {
      this.availableAlgorithms.next(s)
    })
  }

}

