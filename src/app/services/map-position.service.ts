import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { LatLng, latLng, LatLngBounds } from 'leaflet';
import { BehaviorSubject, debounceTime, Subject } from 'rxjs';
import { Route } from '../models/route';
import { MapIds } from './definitions';
import { MapService } from './map.service';

export type MapPosition = { center: BehaviorSubject<LatLng>, zoom: BehaviorSubject<number> }

@Injectable({
  providedIn: 'root'
})
export class MapPositionService {

  // Is set to Leipzig
  defaultCenter: LatLng = latLng(51.34675369387029, 12.374304440972635);
  defaultZoom: number = 13;

  positionLeftMap = {
    center: new BehaviorSubject<LatLng>(this.defaultCenter),
    zoom: new BehaviorSubject<number>(this.defaultZoom)
  }

  positionRightMap = {
    center: new BehaviorSubject<LatLng>(this.defaultCenter),
    zoom: new BehaviorSubject<number>(this.defaultZoom)
  }

  controller: MapIds | null = null

  // Hier wird die Position der zuletzt bewegten Karte gespeichert
  currentCenter: LatLng = this.defaultCenter;
  currentZoom: number = this.defaultZoom;

  syncMaps: boolean = true;

  constructor(
    private snackBar: MatSnackBar,
    private translate: TranslateService,
    private map: MapService
  ) {
    this.map.mapsReady.subscribe(() => {
      this.syncView(this.positionLeftMap, MapIds.leftMap, this.positionRightMap)
      this.syncView(this.positionRightMap, MapIds.rightMap, this.positionLeftMap)
    })
  }

  syncView(posA: MapPosition, mabIdA: MapIds, posB: MapPosition) {
    posA.center.subscribe(c => {
      this.currentCenter = c
      if (this.syncMaps) {
        if (this.controller == mabIdA) {
          setTimeout( () => this.map.getOtherMap(mabIdA).setView(c, posA.zoom.value), 10 )
        }
      }
    })
    posA.zoom.subscribe(z => {
      this.currentZoom = z
      if (this.syncMaps) {
        if (this.controller == mabIdA) {
          setTimeout( () => this.map.getOtherMap(mabIdA).setView(posA.center.value, z), 10 )
        }
      }
    })
  }

  setBoth(center: LatLng, zoom: number) {
    this.map.mapLeft?.setView(center, zoom)
    this.map.mapRight?.setView(center, zoom)
  }

  toggleSync() {
    if (!this.syncMaps) {
      this.setBoth(this.currentCenter, this.currentZoom)
    }
    this.syncMaps = !this.syncMaps
  }

  mouseOver(mapId: MapIds) {
    this.controller = mapId
  }

  /**
   * Diese Methode verwenden, wenn man eine gesamte Route fokussieren will
   */
  flyToRoute(mapId: MapIds, ...routes: Route[]) {
    if (routes.length > 0) {
      let firstPoint = latLng([routes[0].points[0].latitude, routes[0].points[0].longitude])
      let bounds = new LatLngBounds(firstPoint, firstPoint)
      routes.forEach(route => {
        route.points.forEach(r => bounds.extend(latLng([r.latitude, r.longitude])))
      })
      if (mapId.toString() == MapIds.leftMap.toString()) {
        this.map.getLeftMap().fitBounds(bounds.pad(0.25))
      }
      if (mapId.toString() == MapIds.rightMap.toString()) {
        this.map.getRightMap().fitBounds(bounds.pad(0.25))
      }
    }
  }

  setCurrentCenterToUserPosition(): void {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setBoth(latLng(position.coords.latitude, position.coords.longitude), this.currentZoom)
      },
      () => {
        this.displayLocationError()
      },
      { timeout: 20000 }
    );
  }

  displayLocationError(): any {
    this.snackBar.open(
      this.translate.instant('UserLocationError'),
      '',
      { duration: 4000 }
    );
  }

}
