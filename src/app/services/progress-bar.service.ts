import { Injectable } from '@angular/core';
import { ProgressBarMode } from '@angular/material/progress-bar';
@Injectable({
	providedIn: 'root'
})
export class ProgressBarService {
	private mode: ProgressBarMode = 'determinate'

	public value: number = 0
	timeout=0

	constructor() { }

	setDeterminate(value: number = 0){
		setTimeout( () => {
			this.mode = 'determinate'
			this.value = value
			},
			0
		)
	}

	setValue( value: number ) {
		setTimeout( () => {
		this.value += value
			},
			0
		)
	}

	setQuery() {
		setTimeout( () => {
			this.mode = 'query'
			},
			0
		)
	}
	setBuffer() {
		setTimeout( () => {
		this.mode = 'buffer'
			},
			0
		)
	}
	setIndeterminate(){
		setTimeout( () => {
		this.mode = 'indeterminate'
			},
			0
		)
	}

	currentMode(){
		return this.mode
	}
	
	currentValue(){
		return this.value
	}
}
