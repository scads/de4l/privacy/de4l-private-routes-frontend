import {KeycloakService} from "keycloak-angular";

export function initializeKeycloak(keycloak: KeycloakService): () => Promise<boolean> {
  return () =>
    keycloak.init({
      config: {
        url: 'https://auth.de4l.io/auth',
        realm: 'de4l',
        clientId: 'de4l-privacy-service'
      },
      initOptions: {
        onLoad: 'check-sso',
        checkLoginIframe: false,
        checkLoginIframeInterval: 25
      }
    });
}
