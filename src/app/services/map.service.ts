import { Injectable } from '@angular/core';
import { LayerGroup, Map as LMap } from 'leaflet' ;
import { Subject } from 'rxjs';
import { LayersControl } from '../models/layers-control';
import { MapIds } from './definitions';
import { RouteSelectionService } from './selection/route-selection.service';

@Injectable({
	providedIn: 'root'
})
export class MapService {
	// Stellt den Zugriff auf die beiden Leaflet-Maps bereit.

	mapLeft?: LMap;
	mapLeftLayersControl = new LayersControl()
	mapLeftLayers: LayerGroup[] = []

	mapRight?: LMap;
	mapRightLayersControl = new LayersControl()
	mapRightLayers: LayerGroup[] = []

  mapsReady = new Subject<boolean>()

	constructor(
		private selection: RouteSelectionService
	) {
	}


	addMap(id: String, map: LMap){
		map.on( 'click', () => this.onClickHandler() )
		if( id === MapIds.leftMap.valueOf() ) {
			this.mapLeft = map;
		} else if( id === MapIds.rightMap.valueOf() ){
			this.mapRight = map;
		}
    if ( this.mapLeft && this.mapRight ) {
      this.mapsReady.next(true)
    }
	}

	clearAllLayers( mapid: MapIds ){
		var layers = this.getLayers( mapid )
		var length = layers.length;
		for( let i = 0; i<length; i++){
			layers.pop()
		}
	}

	getRightMap(){
		while( this.mapRight == undefined ) { }
		return this.mapRight;
	}

	getLeftMap(){
		while( this.mapLeft == undefined ) { }
		return this.mapLeft;
	}

  getOtherMap( mapId: MapIds ) {
    if (mapId != MapIds.leftMap ) {
      return this.getLeftMap()
    } else {
      return this.getRightMap()
    }
  }

	getLayers(mapid: MapIds): LayerGroup[] {
		if ( mapid.valueOf() == MapIds.leftMap.valueOf()){
			return this.mapLeftLayers;
		} else {
			return this.mapRightLayers;
		}
	}

	getLayersControl(mapid: MapIds): LayersControl {
		if ( mapid.valueOf() == MapIds.leftMap.valueOf()){
			return this.mapLeftLayersControl;
		} else {
			return this.mapRightLayersControl;
		}
	}

	private onClickHandler(){
		this.selection.select();
	}

}

