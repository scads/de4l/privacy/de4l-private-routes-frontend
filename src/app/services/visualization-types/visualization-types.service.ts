import { Injectable } from '@angular/core';
import { LayerGroup } from 'leaflet';
import { LayersControl } from 'src/app/models/layers-control';
import { Route } from 'src/app/models/route';
import { VisualizationDotsService } from './visualization-dots.service';
import { VisualizationHeatmapService } from './visualization-heatmap.service';
import { VisualizationHotlinesWithDotsService } from './visualization-hotlines-with-dots.service';
import { VisualizationHotlinesService } from './visualization-hotlines.service';
import { VisualizationIdwService } from './visualization-idw.service';
import { VisualizationStopsService } from './visualization-stops.service';
import { VisualizationType } from './visualization-type';

@Injectable({
  providedIn: 'root'
})
export class VisualizationTypesService {

  visualizations: VisualizationType[]


  constructor(
    dots: VisualizationDotsService,
    heatmap: VisualizationHeatmapService,
    hotlines: VisualizationHotlinesService,
    private dotsWithHotlines: VisualizationHotlinesWithDotsService,
    private stops: VisualizationStopsService,
    idw: VisualizationIdwService,
  ) {
    this.visualizations = [
      heatmap,
      hotlines,
      dots,
      dotsWithHotlines,
      stops,
      idw
    ]
  }

  /**
   * This returns the visualizationTypes that are added to the map on startup
   */
  public getDefaultVisualizations(): VisualizationType[] {
    return [this.dotsWithHotlines, this.stops]
  }

  /**
   * This returns the LayersControl containing the visualized routes.
   */
  public routesToLayersControl(routes: Route[]): LayersControl {
    const lcs = this.visualizations.map(v => v.routesToLayerControl(routes))
    return lcs.reduce((prev, curr) => prev.merge(curr))
  }

  /**
   * This returns the default LayersControl with empty LayerGroups
   */
  public createDefaultLayersControl(): LayersControl {
    const lcs = this.visualizations.map(v => {
      if (v.addToDefaultLayersControl) {
        return v.routesToLayerControl([])
      } else {
        return new LayersControl()
      }
    })
    return lcs.reduce((prev, curr) => curr.merge(prev), new LayersControl())
  }

}

