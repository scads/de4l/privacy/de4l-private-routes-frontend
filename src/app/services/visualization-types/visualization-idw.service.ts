import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import 'node_modules/leaflet.idw/src/leaflet-idw.js'
import { Route } from 'src/app/models/route';
import { MetricService } from '../metric.service';
import { VisualizationType } from './visualization-type';
import L, { layerGroup } from 'leaflet';
import { LayersControl, NamedLayerGroups } from 'src/app/models/layers-control';
import { Geodata } from 'src/app/models/geodata';
import { IdwOptionsService, IdwSetting } from '../idw-options.service';

@Injectable({
  providedIn: 'root'
})
export class VisualizationIdwService extends VisualizationType {

  constructor(
    metricService: MetricService,
    private translate: TranslateService,
    private idwOptionsService: IdwOptionsService
  ) {
    super(metricService)
    this.addToDefaultLayersControl = false
  }

  name(): string {
    return this.translate.instant('VisualizationIdw');
  }

  public routesToLayerControl(routes: Route[]): LayersControl {
    const nlg: NamedLayerGroups = {}
    // Collect all points from all routes into single array and collect all measurement types
    const allMeasurements: Set<string> = new Set()
    const allPoints = routes.map(r => {
      const p = r.points
      if (p.length > 0) {
        let measurements = p[0].measurements || []
        measurements.forEach(m => allMeasurements.add(m.type))
      }
      return p
    }).flat()

    //Create an IDW overlay for each measurement type
    allMeasurements.forEach(measurement => {
      let options = this.idwOptionsService.getOptionsByName(measurement)
      let data = allPoints.map(p => {
        let measurementValue = this.scaleMeasurement(this.getMeasurement(p, measurement), options )
        return [p.latitude, p.longitude, measurementValue]
      })
      let idw = (L as any).idwLayer(data, options)
      nlg[measurement] = layerGroup([idw])
    })

    return new LayersControl({}, nlg)
  }

  private scaleMeasurement(m: number, options: IdwSetting) {
    let range = Math.abs(options.maxValue - options.minValue)
    return (m - options.minValue) / range
  }

  private getMeasurement(point: Geodata, type: string) {
    let measurement = point.measurements.find(m => m.type === type)
    if (measurement) {
      return measurement.value
    } else {
      return this.defaultValue
    }
  }

}

