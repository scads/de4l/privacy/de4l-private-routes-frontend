import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import L, { LatLng, latLng, LayerGroup } from 'leaflet';
import 'node_modules/leaflet.heat/dist/leaflet-heat.js'
import { Route } from 'src/app/models/route';
import { ColoringService } from '../coloring.service';
import { RouteSelectionService } from '../selection/route-selection.service';
import { VisualizationType } from './visualization-type';
import { MetricService } from 'src/app/services/metric.service';
import { Subject } from 'rxjs';
import { LayersControl, NamedLayerGroups } from 'src/app/models/layers-control';

@Injectable({
  providedIn: 'root'
})
export class VisualizationHeatmapService extends VisualizationType {

  constructor(
    public coloring: ColoringService,
    private translate: TranslateService,
    private selection: RouteSelectionService,
    override metricService: MetricService
  ) {
    super(metricService)
  }

  name(): string {
    return this.translate.instant('VisualizationHeatmap');
  }

  public routesToLayerControl(routes: Route[]): LayersControl {
    const layerGroup = new LayerGroup()
    const deleted = new Subject<boolean>()

    var latLngs: LatLng[] = routes.map(r => r.points).flat().map((point) => {
      return latLng(point.latitude, point.longitude);
    });
    var heatmap = (L as any).heatLayer(
      latLngs,
      {
        // heatmap options
        radius: 35,
        blur: 17,
      }
    )

    // If heatmaps are removed from map -> trigger deleted-subject,
    // so we can unsubscribe from all observables and remove all event listeners
    heatmap.on('removed', () => {
      deleted.next(true)
      deleted.complete()
      heatmap.off()
    })

    layerGroup.addLayer(heatmap)

    const nlg: NamedLayerGroups = {}
    nlg[this.name()] = layerGroup
    return new LayersControl(nlg, {})
  }

}
