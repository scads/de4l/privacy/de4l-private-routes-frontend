import { Injectable } from '@angular/core';
import { GridLayer, LayerGroup, Marker, marker, Popup } from 'leaflet';
import { Stop } from 'src/app/models/stop';
import { Route } from 'src/app/models/route';
import { VisualizationType } from './visualization-type';
import { TranslateService } from '@ngx-translate/core';
import { MetricService } from '../metric.service';
import { LayersControl, NamedLayerGroups } from 'src/app/models/layers-control';

@Injectable({
  providedIn: 'root'
})
export class VisualizationStopsService extends VisualizationType {

  constructor(
    private translate: TranslateService,
    metricService: MetricService
  ) {
    super(metricService)
  }

  name(): string {
    return this.translate.instant('VisualizationStops');
  }

  public routesToLayerControl(routes: Route[]): LayersControl {
    const layerGroup = this.mergeLayerGroups(routes.map(route => this.routeToLayerGroup(route)))
    const nlg: NamedLayerGroups = {}
    nlg[this.name()] = layerGroup
    return new LayersControl({}, nlg)
  }

  private routeToLayerGroup(route: Route): LayerGroup<any> {
    var stops = new LayerGroup();
    route.stops.forEach((stop) => {
      var stopMarker: Marker = marker(
        [stop.latitude, stop.longitude],
        {
          opacity: 0.8
        }
      )
      stopMarker.bindPopup(this.makePopupStop(route, stop))
      stops.addLayer(stopMarker)
    })
    return stops;
  }

  makePopupStop(route: Route, stop: Stop) {
    let data = document.createElement('div')
    let line = document.createElement('div');
    line.innerHTML = "<p><b>POI</b></p>"
    data.appendChild(line)
    data.appendChild(this.popupLine("Route Id", route.id))
    data.appendChild(this.popupLine("Latitude", stop.latitude))
    data.appendChild(this.popupLine("Longitude", stop.longitude))
    return new Popup({}).setContent(data);
  }

}
