import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import L, { LayerGroup, Renderer } from 'leaflet';
import 'node_modules/leaflet-hotline/dist/leaflet.hotline.min.js'
import { Route } from 'src/app/models/route';
import { ColoringService } from '../coloring.service';
import { VisualizationType } from './visualization-type';
import { MetricService } from 'src/app/services/metric.service';
import { RouteSelectionService } from '../selection/route-selection.service';
import { Subject, takeUntil } from 'rxjs';
import { LayersControl, NamedLayerGroups } from 'src/app/models/layers-control';


@Injectable({
  providedIn: 'root'
})
export class VisualizationHotlinesService extends VisualizationType {

  constructor(
    public coloring: ColoringService,
    private translate: TranslateService,
    private selection: RouteSelectionService,
    override metricService: MetricService
  ) {
    super(metricService)
  }

  name(): string {
    return this.translate.instant('VisualizationHotlines');
  }

  public routesToLayerControl(routes: Route[]): LayersControl {
    let renderer = (L as any).Hotline.renderer()
    const lgs: LayerGroup[] = routes.map(route => this.routeToLayerGroup(route, renderer))
    const nlg: NamedLayerGroups = {}
    nlg[this.name()] = new LayerGroup(lgs)
    return new LayersControl(nlg, {})
  }

  private routeToLayerGroup(route: Route, renderer: Renderer): LayerGroup<any> {
    let layerGroup = new LayerGroup();
    let deleted = new Subject<boolean>()

    let data: number[][] = route.points.map((point) =>
      [point.latitude, point.longitude, this.getMetricValueOfPoint(point) / 100]
    );
    var hotlines = (L as any).hotline(
      data,
      {
        renderer: renderer,
        weight: 5,
        opacity: 0.8,
        outlineWidth: 0,
        palette: {
          0.0: this.coloring.calculateColorByMetric(0),
          0.5: this.coloring.calculateColorByMetric(50),
          1.0: this.coloring.calculateColorByMetric(100)
        }
      }
    )

    // If hotlines are removed from map -> trigger deleted-subject,
    // so we can unsubscribe from all observables and remove all event listeners
    hotlines.on('removed', () => {
      deleted.next(true)
      deleted.complete()
      hotlines.off()
    })

    // Create popup for route
    hotlines.bindPopup(this.makePopupRoute(route, this.metricService.basisForVisualization));

    // If hotline is clicked, open the popup and add route to selection
    hotlines.on('click', (e: any) => {
      hotlines.openPopup()
      if (e.originalEvent.ctrlKey) {
        this.selection.toggle(route);
      } else {
        this.selection.select(route);
      }
    })

    // If route is currently selected, change width of hotline
    this.selection.selected.pipe(
      takeUntil(deleted)
    ).subscribe(sel => {
      if (sel.some(selRoute => selRoute.id === route.id)) {
        hotlines.setStyle({ weight: 10 })
      } else {
        hotlines.setStyle({ weight: 5 })
      }
    })

    // If metric for visualization changes -> change coloring and update popup
    this.metricService.metricForVisualizationSubject.pipe(
      takeUntil(deleted)
    ).subscribe(newMetric => {
      let data: number[][] = route.points.map((point) => {
        return [point.latitude, point.longitude, this.getMetricValueOfPoint(point) / 100]
      });
      hotlines.setLatLngs(data);
      const isPopupOpen = hotlines.isPopupOpen()
      hotlines.bindPopup(this.makePopupRoute(route, newMetric))
      if (isPopupOpen) {
        hotlines.openPopup()
      }
    })
    layerGroup.addLayer(hotlines)
    return layerGroup
  }

}
