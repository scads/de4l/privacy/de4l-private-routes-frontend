import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Route } from 'src/app/models/route';
import { VisualizationDotsService } from './visualization-dots.service';
import { VisualizationHotlinesService } from './visualization-hotlines.service';
import { VisualizationType } from './visualization-type';
import { MetricService } from 'src/app/services/metric.service';
import { LayersControl, NamedLayerGroups } from 'src/app/models/layers-control';
import { LayerGroup } from 'leaflet';

@Injectable({
  providedIn: 'root'
})
export class VisualizationHotlinesWithDotsService extends VisualizationType {

  constructor(
    private visualizationDots: VisualizationDotsService,
    private visualizationHotlines: VisualizationHotlinesService,
    private translate: TranslateService,
    override metricService: MetricService
  ) {
    super(metricService)
  }

  name(): string {
    return this.translate.instant('VisualizationHotlinesWithDots');
  }

  public routesToLayerControl(routes: Route[]): LayersControl {
    let hotlines = this.visualizationHotlines.routesToLayerControl(routes).baseLayers[this.visualizationHotlines.name()]
    let dots = this.visualizationDots.routesToLayerControl(routes).baseLayers[this.visualizationDots.name()]
    let nlg: NamedLayerGroups = {}
    nlg[this.name()] = new LayerGroup([...hotlines.getLayers(),...dots.getLayers()])
    return new LayersControl(nlg, {})
  }

}
