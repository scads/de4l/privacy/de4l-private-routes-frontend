import { Injectable } from '@angular/core';
import { circleMarker, LayerGroup } from 'leaflet';
import { Route } from 'src/app/models/route';
import { VisualizationType } from './visualization-type';
import '@angular/localize/init';
import { ColoringService } from '../coloring.service';
import { TranslateService } from '@ngx-translate/core';
import { MetricService } from 'src/app/services/metric.service';
import { RouteSelectionService } from '../selection/route-selection.service';
import { Subject, takeUntil } from 'rxjs';
import { LayersControl, NamedLayerGroups } from 'src/app/models/layers-control';

@Injectable({
  providedIn: 'root'
})
export class VisualizationDotsService extends VisualizationType {

  constructor(
    private coloring: ColoringService,
    private translate: TranslateService,
    private selection: RouteSelectionService,
    override metricService: MetricService
  ) {
    super(metricService)
  }

  name(): string {
    return this.translate.instant('VisualizationDots');
  }

  public routesToLayerControl(routes: Route[]): LayersControl {
    const layerGroup = this.mergeLayerGroups(routes.map(route => this.routeToLayerGroup(route)))
    const nlg: NamedLayerGroups = {}
    nlg[this.name()] = layerGroup
    return new LayersControl(nlg, {})
  }

  private routeToLayerGroup(route: Route): LayerGroup<any> {
    let layerGroup = new LayerGroup();
    let deleted = new Subject<boolean>()

    route.points.forEach((point) => {
      let currentValue = this.getMetricValueOfPoint(point)
      let currentDot = circleMarker(
        [point.latitude, point.longitude],
        {
          fillColor: this.coloring.calculateColorByMetric(currentValue),
          fillOpacity: 0.9,
          color: "#023eab",
          radius: 8,
          stroke: true,
          weight: 2
        }
      );

      // If dots are removed from map -> trigger deleted-subject,
      // so we can unsubscribe from all observables and remove all event listeners
      currentDot.on('removed', () => {
        deleted.next(true)
        deleted.complete()
        currentDot.off()
      })

      // Create popup for point
      currentDot.bindPopup(this.makePopupPoint(point, this.metricService.basisForVisualization, route))

      // If hotline is clicked, open the popup and add route to selection
      currentDot.on('click', (e: any) => {
        currentDot.openPopup()
        if (e.originalEvent.ctrlKey) {
          this.selection.add(route);
        } else {
          this.selection.select(route);
        }
      })

      // If metric for visualization changes -> change coloring and update popup
      this.metricService.metricForVisualizationSubject.pipe(
        takeUntil(deleted)
      ).subscribe(metric => {
        let newValue = this.getMetricValueOfPoint(point)
        currentDot.setStyle({ fillColor: this.coloring.calculateColorByMetric(newValue) });
        const isPopupOpen = currentDot.isPopupOpen()
        currentDot.bindPopup(this.makePopupPoint(point, metric, route))
        if (isPopupOpen) {
          currentDot.openPopup()
        }
      })
      layerGroup.addLayer(currentDot)
    })
    return layerGroup
  }

}
