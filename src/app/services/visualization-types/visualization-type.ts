import { LayerGroup, Popup } from 'leaflet';
import { Geodata } from 'src/app/models/geodata';
import { Route } from 'src/app/models/route';
import { MetricService } from 'src/app/services/metric.service'
import { MetricMetadata } from "src/app/models/metric-metadata";
import { LayersControl, } from 'src/app/models/layers-control';

export abstract class VisualizationType {

  /**
   * The default value for coloring, if no metric is available
   */
  defaultValue = 50
  addToDefaultLayersControl = true

  constructor(
    public metricService: MetricService
  ) { }

  /**
   * This name is displayed in the leaflet-layersControl
   */
  abstract name(): string

  /**
   * Function to create the visualization of a route dataset.
   * Returns the visualized routes in a NamedLayerGroups.
   */
  abstract routesToLayerControl(routes: Route[]): LayersControl

  /**
   * Gets the metric value of the Point according to the selection in basisForVisualization.
   * If no matching metric is found, returns the default value 
   */
  public getMetricValueOfPoint(point: Geodata): number {
    if (point.metrics == null) {
      return this.defaultValue
    }
    if (!this.metricService.basisForVisualization) {
      return this.defaultValue
    }
    let indexOfMetric = point.metrics.findIndex(m => m.id == this.metricService.basisForVisualization.id)
    if (indexOfMetric < 0) {
      return this.defaultValue
    }
    return point.metrics[indexOfMetric].value
  }

  makePopupRoute(route: Route, visualizationMetric: MetricMetadata): Popup {
    let data = document.createElement('div')
    data.appendChild(this.popupLine("Route Id", route.id))
    data.appendChild(this.popupLine("Name", route.name))
    let routeMetricValue;
    if (this.metricService.basisForVisualization) {
      let routeMetric = route.metrics.filter(m => m.id == visualizationMetric.id)[0]
      if (routeMetric != null) {
        routeMetricValue = routeMetric.value;
      } else {
        routeMetricValue = this.defaultValue;
      }
      data.appendChild(this.popupLine(
        this.metricService.basisForVisualization.name,
        Math.round((routeMetricValue + Number.EPSILON) * 100) / 100 + "%"
      ))
      if (visualizationMetric.type === 'privacy' && routeMetric != undefined && routeMetric.performance != undefined) {
        data.appendChild(this.popupLine(
            "Precision",
            (routeMetric.performance.precision == undefined) ? 0 + '%' : Math.round((routeMetric.performance.precision*100 + Number.EPSILON) * 100) / 100 + "%"
        ))
        data.appendChild(this.popupLine(
            "Recall",
            (routeMetric.performance.recall == undefined) ? 0 + '%' : Math.round((routeMetric.performance.recall*100 + Number.EPSILON) * 100) / 100 + "%"
        ))
        data.appendChild(this.popupLine(
            "F-Score",
            (routeMetric.performance.f_score == undefined) ? 0 + '%' : Math.round((routeMetric.performance.f_score*100 + Number.EPSILON) * 100) / 100 + "%"
        ))
      }
    }
    return new Popup({}).setContent(data);
  }

  makePopupPoint(point: Geodata, visualizationMetric: MetricMetadata, route?: Route): Popup {
    let data = document.createElement('div')
    data.appendChild(this.popupLine("Point Id", point.id));
    if (route != undefined) {
      data.appendChild(this.popupLine("Route Id", route.id))
      data.appendChild(this.popupLine("Route name", route.name))
    }
    data.appendChild(this.popupLine("Latitude", Math.round((point.latitude + Number.EPSILON) * 1000000) / 1000000))
    data.appendChild(this.popupLine("Longitude", Math.round((point.longitude + Number.EPSILON) * 1000000) / 1000000))
    if (point.metrics != null && this.metricService.basisForVisualization) {
      let metricName = this.metricService.availablePrivacyMetrics.concat(this.metricService.availableUtilityMetrics)
        .filter(mt => mt.id == visualizationMetric.id).map(mt => mt.name)[0]
      let pointMetric = point.metrics.filter(m => m.id == visualizationMetric.id)[0]
      let currentValue = pointMetric ? pointMetric.value : this.defaultValue
      data.appendChild(this.popupLine(metricName, Math.round((currentValue + Number.EPSILON) * 100) / 100 + "%"))
    }
    point.measurements.forEach(m => {
      data.appendChild(this.popupLine(m.type, Math.round((m.value + Number.EPSILON) * 100) / 100))
    })
    return new Popup({}).setContent(data);
  }

  popupLine(name: string, value: string | number) {
    let line = document.createElement('div');
    line.innerHTML = "<p><b>" + name + ": </b>" + value + "</p>";
    return line
  }

  /**
   * Takes an array of LayerGroups and adds the layers of all groups to one single LayerGroup
   */
  mergeLayerGroups( groups: LayerGroup[] ): LayerGroup {
    if (groups.length == 0 ) {
      return new LayerGroup()
    }
    return groups.reduce((prev, curr) => {
      curr.eachLayer(l => prev.addLayer(l));
      return prev
    },
    )
  }

}
