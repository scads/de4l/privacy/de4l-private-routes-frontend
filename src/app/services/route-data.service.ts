import { Injectable } from '@angular/core';
import { RoutesWithMetrics } from "../models/routes-with-metrics";
import { BehaviorSubject } from 'rxjs';
import { Route } from '../models/route';
import { MetricMetadata } from "../models/metric-metadata";

@Injectable({
  providedIn: 'root'
})
export class RouteDataService {

  constructor() {}

  public publicRoutesSubject = new BehaviorSubject<RoutesWithMetrics | null>(null);
  public privateRoutesSubject = new BehaviorSubject<RoutesWithMetrics | null>(null);
  public currentlyFetchedMetrics: MetricMetadata[] = [];

  public deleteAllData(){
    this.publicRoutesSubject.next(null);
    this.privateRoutesSubject.next(null);
    this.currentlyFetchedMetrics = [];
  }

  public getPrivateRoutes(): Route[] {
    return this.getRoutesNullSafe( this.privateRoutesSubject )
  }

  public getPrivateRoutesWithMetrics(): RoutesWithMetrics {
    return this.getRoutesWithMetricsNullSafe(this.privateRoutesSubject)
  }

  public getPublicRoutes(): Route[] {
    return this.getRoutesNullSafe( this.publicRoutesSubject )
  }

  public getPublicRoutesWithMetrics(): RoutesWithMetrics {
    return this.getRoutesWithMetricsNullSafe(this.publicRoutesSubject)
  }

  private getRoutesNullSafe( s: BehaviorSubject<RoutesWithMetrics | null> ) {
    let value = s.getValue();
    if (!value) {
      return []
    } else if (!value.routes){
      return []
    } else {
      return value.routes
    }
  }

  private getRoutesWithMetricsNullSafe( s: BehaviorSubject<RoutesWithMetrics | null> ) {
    let value = s.getValue();
    if (!value) {
      return new RoutesWithMetrics([], [])
    } else {
      return value
    }
  }

}
