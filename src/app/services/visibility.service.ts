import { Injectable } from '@angular/core';
import { FetchingDataService } from './fetching-data.service';
import { MapService } from './map.service';
import { RouteDataService } from './route-data.service';

@Injectable({
  providedIn: 'root'
})
export class VisibilityService {

  constructor(
    private map: MapService,
    fetchingService: FetchingDataService,
    routeDataService: RouteDataService
  ) {
    // If before first fetch of data, only show the left map
    fetchingService.initialFetching.subscribe( (initialFetching) => {
      if (initialFetching) {
        this.leftMapFullSize()
        this.visibilityOfCenter = false;
      }
    })

    // If public route data is present, show left map and center element
    // If not, show only left map
    routeDataService.publicRoutesSubject.subscribe( (publicRoutes) => {
      if( publicRoutes == null ) {
      } else if ( publicRoutes.routes.length > 0 ){
        this.visibilityOfLeftMap = true;
        setTimeout( () => this.map.getLeftMap().invalidateSize(), 200 )
        this.visibilityOfCenter = true
      } else {
        this.leftMapFullSize()
        this.visibilityOfCenter = false
      }
    } )

    // If private route data is present show left map, center element and right map
    // If not, hide right map
    routeDataService.privateRoutesSubject.subscribe( (privateRoutes) => {
      if( privateRoutes == null ) {
      } else if ( privateRoutes.routes.length > 0 ){
        this.setFullVisibility()
      } else {
        this.visibilityOfRightMap = false
      }
    })

  }

  drawerOpen: boolean = false

  visibilityOfLeftMap: boolean = true;
  visibilityOfRightMap: boolean = false;
  visibilityOfCenter: boolean = false;

  public setFullVisibility() {
    this.visibilityOfLeftMap = true;
    this.visibilityOfRightMap = true;
    this.visibilityOfCenter = true;
    this.normalMapSizes();
  }

  public normalMapSizes() {
    this.visibilityOfLeftMap = true;
    this.visibilityOfRightMap = true;
    setTimeout( () => this.map.getRightMap().invalidateSize(), 200)
    setTimeout( () => this.map.getLeftMap().invalidateSize(), 200 )
  }
  public rightMapFullSize(){
    this.visibilityOfRightMap = true;
    this.visibilityOfLeftMap = false;
    setTimeout( () => this.map.getRightMap().invalidateSize(), 200)
  }

  public leftMapFullSize(){
    this.visibilityOfRightMap = false;
    this.visibilityOfLeftMap = true;
    setTimeout( () => this.map.getLeftMap().invalidateSize(), 200 )
  }
}
