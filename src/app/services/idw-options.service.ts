import { Injectable } from '@angular/core';
import { BehaviorSubject, map } from 'rxjs';
import { ColoringService } from './coloring.service';
import { RouteOverviewService } from './route-overview.service';

export interface IdwSetting {
  opacity: number,
  cellSize: number,
  exp: number,
  minValue: number,
  maxValue: number,
  max: number,
  gradient: { [key: number]: string }
}

@Injectable({
  providedIn: 'root'
})
export class IdwOptionsService {

  private defaultSetting: IdwSetting = { max:1, opacity: 0.9, cellSize: 17, exp: 8, minValue: 15, maxValue: 22, gradient: {} }

  currentSettings: BehaviorSubject<Map<string, IdwSetting>> = new BehaviorSubject(new Map())

  constructor(
    routeOverviewService: RouteOverviewService,
    coloring: ColoringService
  ) {
    this.defaultSetting.gradient = {
      0.0: coloring.calculateColorByMetric(100),
      0.5: coloring.calculateColorByMetric(50),
      1.0: coloring.calculateColorByMetric(0)
    }
    routeOverviewService.routes.pipe(
      map(routes => routes.map(r => r.measurements).flat()),
      map(ms => {
        let mSet = new Set<string>
        ms.forEach(m => mSet.add(m))
        return mSet
      })
    ).subscribe(measurements => this.registerMeasurements(measurements))
  }

  newSetting(name: string, $event: IdwSetting) {
    const current = this.currentSettings.getValue()
    current.set(name, $event)
    this.currentSettings.next(current)
  }

  getOptionsByName(measurement: string): any {
    return this.currentSettings.getValue().get(measurement)
  }

  registerMeasurements(allMeasurements: Set<string>) {
    this.currentSettings.next(this.measurementsToSettingsMap(allMeasurements))
  }

  private measurementsToSettingsMap(allMeasurements: Set<string>) {
    let map = new Map<string, IdwSetting>()
    allMeasurements.forEach(m => {
      map.set(m, this.getDefaultSetting())
    })
    return map
  }

  getDefaultSetting(): IdwSetting {
    return { ...this.defaultSetting }
  }

}
