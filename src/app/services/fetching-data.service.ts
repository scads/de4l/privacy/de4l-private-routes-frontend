import { Injectable } from '@angular/core';
import { MetricMetadata } from '../models/metric-metadata';
import { RouteOverviewSelectionService } from './selection/route-overview-selection.service';
import { RouteOverviewService as RouteOverviewService } from './route-overview.service';
import { RoutesWithMetrics } from "../models/routes-with-metrics";
import { CommunicationService } from './communication.service';
import { PublicAndPrivateRoutes } from "../models/public-and-private-routes";
import { ProgressBarService } from './progress-bar.service';
import { RouteDataService } from './route-data.service';
import { MetricService } from './metric.service';
import { PrivacyService } from './privacy.service';
import { BehaviorSubject } from 'rxjs';
import { PrivacyAlgorithmSetting } from '../models/PrivacyAlgorithmSetting';

@Injectable({
	providedIn: 'root'
})
export class FetchingDataService {

	public initialFetching = new BehaviorSubject<boolean>(true)
	public newRouteSelection = true

    constructor(
        public routeOverviewSelection: RouteOverviewSelectionService,
        private databaseOverview: RouteOverviewService,
        private communication: CommunicationService,
        private progressBar: ProgressBarService,
        private routeDataService: RouteDataService,
        private metricService: MetricService,
        private privacyService: PrivacyService,
    ) {
      this.metricService.selectedMetricsSubject.subscribe( selectedMetrics => {
        if (!selectedMetrics.every(m => this.routeDataService.currentlyFetchedMetrics.includes(m))) {
            if ( this.initialFetching.getValue() ) {
              this.fetchPublicData();
            }
            else {
              this.updatedMetrics();
            }
        }
      })

      this.privacyService.currentSetting.subscribe( algorithm => {
        this.updatedPrivacyAlgorithm( algorithm );
      });

      this.databaseOverview.displayedRoutes.subscribe( selection => {
        this.newRouteSelection = true
        if (selection.length > 0) {
          let newSetting = this.privacyService.currentSetting.getValue()
          if ( this.initialFetching.getValue() || newSetting == null ) {
            this.fetchPublicData();
          } else {
            this.fetchAllData(newSetting);
          } 
        } else {
          routeDataService.deleteAllData();
          this.initialFetching.next( true )
        }
      })
    }

	async updateRouteData(data: PublicAndPrivateRoutes): Promise<void> {
        this.routeDataService.publicRoutesSubject.next(data.nonPrivateRoutes);
        this.routeDataService.privateRoutesSubject.next(data.privateRoutes);
    }

    async updatePublicRouteData(publicRoutes: RoutesWithMetrics): Promise<void> {
        this.routeDataService.publicRoutesSubject.next(publicRoutes);
    }

    async updatePrivateRouteData(privateRoutes: RoutesWithMetrics): Promise<void> {
        this.routeDataService.privateRoutesSubject.next(privateRoutes);
    }

    async fetchAllData( newSetting: PrivacyAlgorithmSetting ): Promise<PublicAndPrivateRoutes> {
        this.initialFetching.next( false )
        this.progressBar.setQuery()
        let allData = await this.communication.getPublicAndPrivateData(this.routeOverviewSelection.currentlySelectedIds(),
            newSetting.algorithm.id,
            newSetting.parameters,
            this.getMetricIds(this.metricService.currentPrivacyMetrics.concat(this.metricService.currentUtilityMetrics)));
        this.updateRouteData(allData);
        this.progressBar.setDeterminate()
        return allData;
    }

    async fetchPublicData(): Promise<RoutesWithMetrics> {
        this.initialFetching.next( false )
        this.progressBar.setQuery()
        let publicRoutes = await this.communication.getPublicData(this.routeOverviewSelection.currentlySelectedIds(),
            this.getMetricIds(this.metricService.currentPrivacyMetrics.concat(this.metricService.currentUtilityMetrics)));
        this.updatePublicRouteData(publicRoutes);
        this.progressBar.setDeterminate()
        return publicRoutes;
    }

    async updatedMetrics(): Promise<PublicAndPrivateRoutes> {
        this.initialFetching.next( false )
        this.progressBar.setQuery()
        let allData = await this.communication.getNewMetrics(
            new PublicAndPrivateRoutes(this.routeDataService.getPublicRoutesWithMetrics(), this.routeDataService.getPrivateRoutesWithMetrics()),
            this.routeOverviewSelection.currentlySelectedIds(),
            this.getMetricIds(this.metricService.currentPrivacyMetrics.concat(this.metricService.currentUtilityMetrics)));
        this.updateRouteData(allData);
        this.progressBar.setDeterminate()
        return allData;
    }

    updatedPrivacyAlgorithm( newSetting: PrivacyAlgorithmSetting | null )  {
        this.initialFetching.next( false )
        this.progressBar.setQuery()
        if ( newSetting != null ) {
          this.communication.getPrivateData(this.routeOverviewSelection.currentlySelectedIds(),
            newSetting.algorithm.id,
            newSetting.parameters,
            this.getMetricIds(this.metricService.currentPrivacyMetrics.concat(this.metricService.currentUtilityMetrics))
          ).then( privateRoutes => {
            this.updatePrivateRouteData(privateRoutes)
            this.progressBar.setDeterminate()
          } )
        }
        return
    }

    private getMetricIds(metrics: MetricMetadata[]): number[] {
        return metrics.map(metric => {return metric.id});
    }

}

