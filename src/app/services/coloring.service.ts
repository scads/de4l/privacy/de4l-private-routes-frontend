import {VisualizationTypes} from "./definitions";
import {Injectable} from "@angular/core";
import { Route } from 'src/app/models/route';
import { Metric } from 'src/app/models/metric';

/** GENERAL CLASS for ColoringService on UI */
@Injectable({
    providedIn: 'root'
})
export class ColoringService {

    public calculateColorByMetric(percentage: number): string {
        return 'rgb(' + (100-percentage) + '%, ' + (percentage-(percentage*0.5)) + '%, ' + (percentage*0.5) + '%)';
    }
}
