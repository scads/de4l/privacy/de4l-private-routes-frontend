import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Location } from '@angular/common';
import { Route } from "../models/route";
import { RoutesWithMetrics } from "../models/routes-with-metrics";
import { PublicAndPrivateRoutes } from "../models/public-and-private-routes";
import { KeycloakService } from "keycloak-angular";
import { MatDialog } from '@angular/material/dialog';
import { RouteOverview } from '../models/route-overview';
import { catchError, Observable, tap, throwError, of, concat, toArray, firstValueFrom, map } from 'rxjs';
import { ProgressBarService } from './progress-bar.service';
import { environment } from 'src/environments/environment';
import { PrivacyAlgorithmMetadata } from '../models/PrivacyAlgorithmMetadata';
import { MetricMetadata } from '../models/metric-metadata';
import { LoadingErrorComponent } from '../components/dialogs/loading-error/loading-error.component';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': environment.keycloak ? 'Bearer ' + this.keycloakService.getKeycloakInstance().token : ""
    })
  };

  // Wird mit reverse proxy zum Backend umgeleitet
  // siehe proxy.conf.json
  baseUrl = this.location.prepareExternalUrl('api')
  inputError = ""

  constructor(
    private client: HttpClient,
    private keycloakService: KeycloakService,
    private progressBar: ProgressBarService,
    private location: Location,
    private dialog: MatDialog,
  ) { }

  public getAvailableAlgorithms(): Observable<PrivacyAlgorithmMetadata[]> {
    const url = `${this.baseUrl}/privacyAlgorithm`
    return this.client.get<PrivacyAlgorithmMetadata[]>(url, this.httpOptions).pipe(
      catchError((s) => {
        this.handleError(s)
        return of([])
      }),
    )
  }

  public getAvailableMetrics(): Observable<MetricMetadata[]> {
    const url = `${this.baseUrl}/metric`
    return this.client.get<MetricMetadata[]>(url, this.httpOptions).pipe(
      catchError((s) => {
        this.handleError(s)
        return of([])
      }),
    )
  }

  public getOverview(): Observable<RouteOverview[]> {
    const url = `${this.baseUrl}/route/overview`
    this.progressBar.setQuery()
    return this.client.get<RouteOverview[]>(url, this.httpOptions).pipe(
      catchError((s) => {
        this.handleError(s)
        return of([])
      }),
      tap(() => this.progressBar.setDeterminate()),
    );
  }

  public deleteRoute(routeId: Number): Observable<unknown> {
    const url = `${this.baseUrl}/route/${routeId}`
    this.progressBar.setQuery()
    return this.client.delete(url, this.httpOptions).pipe(
      catchError((e) => {
        this.handleError(e)
        return of(false)
      }),
      tap(() => this.progressBar.setDeterminate()),
    );
  }

  public dropPoints(routeId: Number, everyNth: Number) {
    const url = `${this.baseUrl}/route/${routeId}/dropEvery/${everyNth}`
    this.progressBar.setQuery()
    return this.client.delete(url, this.httpOptions).pipe(
      catchError((e) => {
        this.handleError(e)
        return of(false)
      }),
      tap(() => this.progressBar.setDeterminate()),
    );
  }

  public importData<T>(data: T[], endpoint: string) {
    try {
        const url = `${this.baseUrl}/route/add/${endpoint}`
        this.progressBar.setQuery()
        let observables = data.map(d => this.client.post(url, d, this.httpOptions).pipe(catchError(e => of(e))))
        return concat(...observables).pipe(
          toArray(),
          tap(() => this.progressBar.setDeterminate())
        )
    } catch (error: any) {
        this.inputError = " ";
        console.log("ERROR:", error.message);
        return of([]);
    }
  }

  public getPublicRouteFromDb(routeId: number): Observable<Route> {
    const url = `${this.baseUrl}/route/${routeId}`
    return this.client.get<Route>(url, this.httpOptions)
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      console.error('An error occurred:', error.error);
    } else {
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error, error);
    }
    return throwError(() => new Error('Something bad happened; please try again later.'));
  }

  async getPublicAndPrivateData(routeIds: Array<number>, privacyAlgorithm: number, privacyParameter: Array<number>, metricIds: Array<number>): Promise<PublicAndPrivateRoutes> {
    let address = this.baseUrl + '/route/publicAndPrivateRoutes' +
      '?routeIds=' + routeIds +
      '&requestedPrivacyAlgorithm=' + privacyAlgorithm +
      '&parameterValue=' + privacyParameter +
      '&metricIds=' + metricIds +
      '&calculateNonPrivateMetrics=' + true;
    return this.client.get<PublicAndPrivateRoutes>(address, this.httpOptions).toPromise().then((res: any) => {
      if (res.nonPrivateRoutes == null) {
        this.dialog.open(LoadingErrorComponent, {})
      }
      return res;
    }).catch(() => {
      this.dialog.open(LoadingErrorComponent, {})
    })

  }

  async getPublicData(routeIds: Array<number>, metricIds: Array<number>): Promise<RoutesWithMetrics> {
    let address = this.baseUrl + '/route/publicRoutes' +
      '?routeIds=' + routeIds +
      '&requestedPrivacyAlgorithm=' +
      '&parameterValue=' +
      '&metricIds=' + metricIds +
      '&calculateNonPrivateMetrics=' + true;
    return this.client.get<PublicAndPrivateRoutes>(address, this.httpOptions).toPromise().then((res: any) => {
      if (res.nonPrivateRoutes == null) {
        this.dialog.open(LoadingErrorComponent, {})
      }
      return res.nonPrivateRoutes;
    }).catch(() => {
      this.dialog.open(LoadingErrorComponent, {})
    })
  }

  async getPrivateData(routeIds: Array<number>, privacyAlgorithm: number, privacyParameter: Array<number>, metricIds: Array<number>): Promise<RoutesWithMetrics> {
    let address = this.baseUrl + '/route/privateRoutes' +
      '?routeIds=' + routeIds +
      '&requestedPrivacyAlgorithm=' + privacyAlgorithm +
      '&parameterValue=' + privacyParameter +
      '&metricIds=' + metricIds +
      '&calculateNonPrivateMetrics=' + false;
    return this.client.get<PublicAndPrivateRoutes>(address, this.httpOptions).toPromise().then((res: any) => {
      if (res.privateRoutes == null) {
        this.dialog.open(LoadingErrorComponent, {})
      }
      return res.privateRoutes;
    }).catch(() => {
      this.dialog.open(LoadingErrorComponent, {})
    })
  }

  async getNewMetrics(publicAndPrivateRoutes: PublicAndPrivateRoutes, routeIds: Array<number>, metricIds: Array<number>): Promise<PublicAndPrivateRoutes> {
    let address = this.baseUrl + '/route/changedMetrics' +
      '?routeIds=' + routeIds +
      '&requestedPrivacyAlgorithm=' +
      '&parameterValue=' +
      '&metricIds=' + metricIds +
      '&calculateNonPrivateMetrics=' + true;
    let body = JSON.stringify(publicAndPrivateRoutes);
    return this.client.post<PublicAndPrivateRoutes>(address, body, this.httpOptions).toPromise().then((res: any) => {
      if (res.nonPrivateRoutes == null) {
        this.dialog.open(LoadingErrorComponent, {})
      }
      return res;
    }).catch(() => {
      this.dialog.open(LoadingErrorComponent, {})
    })
  }

}
