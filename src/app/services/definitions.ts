import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Definitions {
}

export enum MapIds {
	leftMap = 'Public data',
	rightMap = 'Protected data'
}

export enum Direction {
  Left = 'left',
  Right = 'right',
}

export enum Progress {
  Determine = 'determinate',
  Query = 'query'
}

export enum VisualizationTypes {
  Dots = 'Dots',
  Polyline = 'Polyline',
  Heatmap = 'Heatmap',
  Marker = 'Marker',
  PolylinesWithDots = 'Polylines with Dots',
}

export enum LocationType {
  Route = 'route',
  SinglePoint = 'point'
}
