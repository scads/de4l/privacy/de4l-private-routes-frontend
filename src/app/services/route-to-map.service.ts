import { Injectable } from '@angular/core';
import { Route } from '../models/route';
import { MapIds } from './definitions';
import { MapService } from './map.service';
import { MapPositionService } from './map-position.service';
import { FetchingDataService } from './fetching-data.service';
import { RouteDataService } from './route-data.service';
import { first } from 'rxjs/operators';
import { VisualizationTypesService } from './visualization-types/visualization-types.service';
import { IdwOptionsService } from './idw-options.service';


@Injectable({
  providedIn: 'root'
})
export class RouteToMapService {

  basicDataIsShown: boolean = false;
  routeStopDataIsShown: boolean = false;

  constructor(
    private mapPositionService: MapPositionService,
    private visualizationTypes: VisualizationTypesService,
    private mapService: MapService,
    private fetchingService: FetchingDataService,
    routeDataService: RouteDataService,
    private idwOptions: IdwOptionsService
  ) {
    // Initialize the layersControls of both maps
    routeDataService.publicRoutesSubject.pipe(first(r => r != null)).subscribe(() => {
      [MapIds.rightMap, MapIds.leftMap].forEach(mapid => {
        this.initMap(mapid)
      })
    })

    // New original data available -> update left map
    routeDataService.publicRoutesSubject.subscribe((r) => {
      if (r) {
        if (this.fetchingService.newRouteSelection) {
          this.refreshMap(MapIds.leftMap, r.routes, true)
        }
        else {
          this.refreshMap(MapIds.leftMap, r.routes, false)
        }
      }
      else {
        var emptyRoutes: Route[] = []
        this.refreshMap(MapIds.leftMap, emptyRoutes, false)
      }
    })

    // New private data available -> update right map
    routeDataService.privateRoutesSubject.subscribe((r) => {
      if (r) { this.refreshMap(MapIds.rightMap, r.routes, false) }
      else {
        var emptyRoutes: Route[] = []
        this.refreshMap(MapIds.rightMap, emptyRoutes, false)
      }
    })
  }

  /**
   * Initializes the maps with a default LayersControl
   */
  private initMap(mapId: MapIds) {
    const defaultLc = this.visualizationTypes.createDefaultLayersControl()
    const lc = this.mapService.getLayersControl(mapId).merge(defaultLc)
    const defaultVisualizations = this.visualizationTypes.getDefaultVisualizations()
    defaultVisualizations.forEach(o => {
      this.mapService.getLayers(mapId).push(lc.getLayerGroupByName(o.name()))
    })
  }

  /**
   * Resets the LayersControl of the map.
   * This clears all the LayerGroups and then adds the new routes to the map.
   * If flyToRoute = true -> map focusses the added routes
   */
  public refreshMap(mapId: MapIds, routes: Route[], flyToRoutes = false) {
    this.mapService.getLayersControl(mapId).clear()
    if (routes.length == 0) {
      this.basicDataIsShown = false
      return
    }
    this.basicDataIsShown = true
    this.addRoutesToMap(routes, mapId)
    if (flyToRoutes) {
      this.mapPositionService.flyToRoute(mapId, ...routes)
    }
    this.fetchingService.newRouteSelection = false
  }

  private addRoutesToMap(routes: Route[], mapId: MapIds) {
    const lc = this.visualizationTypes.routesToLayersControl(routes)
    this.mapService.getLayersControl(mapId).merge(lc)
  }

}

