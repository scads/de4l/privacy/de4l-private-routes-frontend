import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatStepperModule } from '@angular/material/stepper';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTreeModule } from '@angular/material/tree';
import { MatCardModule } from '@angular/material/card';
import { MatSliderModule } from '@angular/material/slider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PublicComponent } from "./views/public/public.component";
import { LeafletMapComponent } from './components/map/leaflet-map/leaflet-map.component';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found.component';
import { initializeKeycloak } from 'src/app/services/keycloak.service'
import { AuthGuard } from "./services/guard.service";
import { TitlebarComponent } from './components/titlebar/titlebar.component';
import { PrivateRoutesComponent } from './views/private-routes/private-routes.component';
import { PrivateRoutesFooterComponent } from './components/private-routes-footer/private-routes-footer.component';
import { CentralInformationComponent } from './components/central-information/central-information.component';
import { FetchingDataService } from './services/fetching-data.service';
import { RouteDataService } from './services/route-data.service';
import { MapService } from './services/map.service';
import { MetricViewerComponent } from './components/metric-viewer/metric-viewer.component';
import { MetricSettingsComponent } from './components/metric-settings/metric-settings.component';
import { ImportDataComponent } from './components/dialogs/import-data/import-data.component';
import { ExportDataComponent } from './components/dialogs/export-data/export-data.component';
import { MenuComponent } from './components/menu/menu.component';
import { MapHeaderComponent } from './components/map/map-header/map-header.component';
import { MapComponent } from './components/map/map.component';
import { RouteOverviewComponent } from "./components/route-overview/RouteOverviewComponent";
import { RouteOverviewTableComponent } from './components/route-overview/route-overview-table/route-overview-table.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatPaginatorModule } from '@angular/material/paginator';
import { DeleteRouteDialogComponent } from './components/dialogs/delete-route-dialog/delete-route-dialog.component';
import { DropPointsDialogComponent } from './components/dialogs/drop-points-dialog/drop-points-dialog.component';
import { DateFilterComponent } from './components/route-overview/route-overview-filter/date-filter/date-filter.component';
import { RouteOverviewFilterComponent } from './components/route-overview/route-overview-filter/route-overview-filter.component';
import { IdFilterComponent } from './components/route-overview/route-overview-filter/id-filter/id-filter.component';
import { PointFilterComponent } from './components/route-overview/route-overview-filter/point-filter/point-filter.component';
import { StepwidthFilterComponent } from './components/route-overview/route-overview-filter/stepwidth-filter/stepwidth-filter.component';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { NameFilterComponent } from './components/route-overview/route-overview-filter/name-filter/name-filter.component';
import { SelectedFilterComponent } from './components/route-overview/route-overview-filter/selected-filter/selected-filter.component';
import { TruncateModule } from '@yellowspot/ng-truncate';
import { ImportFeedbackComponent } from './components/dialogs/import-feedback/import-feedback.component';
import { LoadingErrorComponent } from './components/dialogs/loading-error/loading-error.component';
import { PointDisplayWarningComponent } from './components/dialogs/point-display-warning/point-display-warning.component';
import { environment } from 'src/environments/environment';
import { PrivacyAlgorithmSettingsComponent } from './components/privacy-algorithms/privacy-algorithm-settings/privacy-algorithm-settings.component';
import { PrivacyAlgorithmSelectorComponent } from './components/privacy-algorithms/privacy-algorithm-selector/privacy-algorithm-selector.component';
import { HelpComponent } from './components/dialogs/help/help.component';
import { IdwOptionsButtonComponent } from './components/idw-options/idw-options-button.component';
import { IdwOptionsComponent } from './components/idw-options/idw-options.component';
import { IdwOptionsFormComponent } from './components/idw-options/idw-options-form.component';

@NgModule({
  declarations: [
    AppComponent,
    PublicComponent,
    LeafletMapComponent,
    PageNotFoundComponent,
    TitlebarComponent,
    PrivateRoutesComponent,
    PrivateRoutesFooterComponent,
    CentralInformationComponent,
    MetricViewerComponent,
    MetricSettingsComponent,
    ImportDataComponent,
    ExportDataComponent,
    MenuComponent,
    MapHeaderComponent,
    IdwOptionsButtonComponent,
    MapComponent,
    RouteOverviewComponent,
    RouteOverviewTableComponent,
    RouteOverviewFilterComponent,
    PrivacyAlgorithmSelectorComponent,
    PrivacyAlgorithmSettingsComponent,
    DeleteRouteDialogComponent,
    DropPointsDialogComponent,
    DateFilterComponent,
    IdFilterComponent,
    PointFilterComponent,
    StepwidthFilterComponent,
    NameFilterComponent,
    SelectedFilterComponent,
    ImportFeedbackComponent,
    LoadingErrorComponent,
    PointDisplayWarningComponent,
    HelpComponent,
    IdwOptionsComponent,
    IdwOptionsFormComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatSidenavModule,
    TruncateModule,
    AppRoutingModule,
    MatProgressSpinnerModule,
    KeycloakAngularModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    LeafletModule,
    NgxMatFileInputModule,
    MatSortModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    NgxSliderModule,
    MatProgressBarModule,
    MatButtonToggleModule,
    MatInputModule,
    MatSnackBarModule,
    HttpClientModule,
    MatSelectModule,
    MatMenuModule,
    MatDialogModule,
    MatTreeModule,
    MatCardModule,
    MatListModule,
    MatSliderModule,
    MatTooltipModule,
    MatTableModule,
    MatTabsModule,
    MatExpansionModule,
    MatGridListModule,
    MatStepperModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoader,
        deps: [HttpClient]
      }
    }),
    ReactiveFormsModule
  ],
  providers: [
    RouteDataService,
    MapService,
    FetchingDataService,
    //@ts-ignore
  ].concat(environment.keycloak ? [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService]
    },
    AuthGuard
  ] : []),

  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(translate: TranslateService) {
    translate.addLangs(['en', 'de']);
    translate.setDefaultLang('en');
    translate.use('en');
  }
}

export function httpTranslateLoader(http: HttpClient): any {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}
