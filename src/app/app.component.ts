import {Component, OnInit} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ImportDataComponent } from './components/dialogs/import-data/import-data.component';
import { RouteOverviewService } from './services/route-overview.service';
import { ProgressBarService } from './services/progress-bar.service';
import { environment } from 'src/environments/environment';
import { FetchingDataService } from './services/fetching-data.service';
import { PrivacyService } from './services/privacy.service';
import { MetricService } from './services/metric.service';
import { HelpComponent } from './components/dialogs/help/help.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(
    public progressBar: ProgressBarService,
    public routeOverview: RouteOverviewService,
    private dialog: MatDialog,
    // FetchingDataService muss in einem Component importiert werden, damit es
    // instanziiert wird
    private fetchingService: FetchingDataService,
    public privacyService: PrivacyService,
    public metricService: MetricService
  ) {
  }

  ngOnInit(): void {
    this.routeOverview.updateRouteOverviews()
    this.privacyService.fetchAvailableAlgorithms()
    this.metricService.fetchAvailableMetrics()
    if ( environment.helpDialogOnStartup ) {
      this.dialog.open(HelpComponent)
    }
    if ( environment.import && environment.importWindowOnStartup ) {
      this.dialog.open(ImportDataComponent, {
        panelClass: 'app-full-bleed-dialog' 
      });
    }
  }

}
