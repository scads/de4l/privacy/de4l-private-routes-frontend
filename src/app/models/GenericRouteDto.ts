export interface GenericRouteDto {
    name: string
    date: string
    route: number[][]
    stops: number[]
		timestamps: string[]
    measurements: { type: string, value: number }[] | null
}
