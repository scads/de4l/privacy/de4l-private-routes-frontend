import { RoutesWithMetrics } from "./routes-with-metrics";

export class PublicAndPrivateRoutes {
    nonPrivateRoutes: RoutesWithMetrics;
    privateRoutes: RoutesWithMetrics;

    constructor(nonPrivateRoutes: RoutesWithMetrics, privateRoutes: RoutesWithMetrics) {
        this.nonPrivateRoutes = nonPrivateRoutes;
        this.privateRoutes = privateRoutes;
    }
}

