export interface PrivacyAlgorithmMetadata {

  /**
   * Die Id des Algorithmus.
   * Wird bei bei Privatisierungsanfragen mitgegeben.
   */
  id: number

  /**
   * Der Name des Algorithmus
   */
  name: string

  /**
   * Geordnete Liste der Parameter des Algorithmus.
   * Die Reihenfolge ist wichtig, da bei einer Privatisierungsanfrage, 
   * die Parameter in der gleichen Reihenfolge übergeben werden müssen
   */
  parameters: PrivacyAlgorithmParameter[]

  /**
   * Beschreibung des Algorithmus.
   * Was macht er mit einer Route
   */
  description: string

}

export interface PrivacyAlgorithmParameter {

  /**
   * Der Name des Parameters.
   * Zum Beispiel: Alpha
   */
  name: string

  /**
   * Das Einheitszeichen.
   * Zum Beispiel: m
   */
  unit: string

  /**
   * Soll die Scala logarithmisch sein oder nicht
   */
  logScale: boolean

  /**
   * Der minimale Wert des Parameters.
   * Bei diesem Wert sollte die Privatisierung am schwächsten sein.
   */
  min: number

  /**
   * Der maximale Wert des Parameters.
   * Bei diesem Wert soll die Privatisierung am stärksten sein.
   * Sollte so gewählt werden, dass die Laufzeit des Algorithmus nicht zu groß wird.
   */
  max: number

  /**
   * Beschreibung des Parameters,
   * sie sollte dem User vermitteln, was mit diesem Parameter eingestellt werden kann.
   */
  description: string

}

