export interface Measurement {
  type: string
  value: number
}
