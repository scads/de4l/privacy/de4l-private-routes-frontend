import { Route } from "./route";
import { Metric } from "./metric";

export class RoutesWithMetrics {
    routes: Route[];
    metrics: Metric[];

    constructor(routes: Route[], metrics: Metric[]) {
        this.routes = routes;
        this.metrics = metrics;
    }
}