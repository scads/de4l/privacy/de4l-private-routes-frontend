export interface TaxiRouteDto {
	TRIP_ID: string,
	TIMESTAMP: number,
	POLYLINE: string,
	stops: string
}
