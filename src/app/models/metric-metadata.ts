export interface MetricMetadata {
  name: string;
  type: MetricMetadataType;
  id: number;
  description: string;
}

enum MetricMetadataType {
  Privacy = "privacy",
  Utility = "utility",
  Measurement = "measurement"
}
