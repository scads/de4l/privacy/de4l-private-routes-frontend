import { LayerGroup } from "leaflet";
import { Route } from "./route";

/**
* Repräsentiert die Darstellung einer Route auf einer leaflet-Karte.
* Enthält die Route und für jeden Visualisierungstyp eine LayerGroup, die die Marker/Polylines/etc
* enthält, die nötig sind um die Route anzuzeigen.
* Die Stop-Punkte der Route werden gesondert abgespeichert, um sie ein- und ausblenden zu können
*/
export interface RouteVisualization {
	route: Route,
	baseLayers: {
		[key: string]: LayerGroup
	}
	overlays: {
		[key: string]: LayerGroup
  }
}
