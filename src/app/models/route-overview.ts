export interface RouteOverview {
  id: number;
  name: string;
  date: Date;
  pointCount: number;
  stepwidth: number;
  measurements: string[];
}
