import { LayerGroup } from "leaflet";

export type NamedLayerGroups = { [key: string]: LayerGroup }

export class LayersControl {
  baseLayers: NamedLayerGroups
  overlays: NamedLayerGroups

  constructor(baseLayers: NamedLayerGroups = {}, overlays: NamedLayerGroups = {}) {
    this.overlays = overlays
    this.baseLayers = baseLayers
  }

  /**
   * Takes a LayersControl and adds all the NamedLayerGroups to this.
   * If the LayersControl has an entry with the same name, the content of the LayerGroup is added 
   */
  merge(lc: LayersControl): LayersControl {
    this.mergeNamedLayerGroups(lc.overlays, this.overlays)
    this.mergeNamedLayerGroups(lc.baseLayers, this.baseLayers)
    return this
  }

  mergeNamedLayerGroups(nlg: NamedLayerGroups, into: NamedLayerGroups): NamedLayerGroups {
    for (const key in nlg) {
      if (into[key]) {
        nlg[key].eachLayer(l => into[key].addLayer(l))
      } else {
        into[key] = nlg[key]
      }
    }
    return into
  }

  /**
   * Deletes all Layers from the LayerGroups inside the overlays and baseLayers
   * and removes all the LayerGroups that are not in the default NamedLayerGroups
   */
  clear() {
    this.clearNamedLayerGroups(this.baseLayers)
    this.clearNamedLayerGroups(this.overlays)
  }

  /**
   * Removes all the layers inside of nlg and triggers the 'removed'-event,
   * so the VisualizationType can cleanup subscriptions and event listeners
   */
  private clearNamedLayerGroups(nlg: NamedLayerGroups) {
    for (const key in nlg) {
      nlg[key].fire('removed')
      nlg[key].eachLayer(l => {
        l.fire("removed")
        l.remove()
      })
      nlg[key].clearLayers()
    }
  }

  getLayerGroupByName( name: string )  {
    return ({...this.baseLayers, ...this.overlays})[name]
  }
  
  getAllLayerGroups() {
    const lgs = []
    for ( let key in this.overlays ) {
      lgs.push(this.overlays[key])
    }
    for ( let key in this.baseLayers ) {
      lgs.push(this.baseLayers[key])
    }
    return lgs
  }

}
