export class MetricPerformanceMeasures {
    public precision: number;
    public recall: number;
    public f_score: number;

    constructor(precision: number, recall: number, f_score: number) {
        this.precision = precision;
        this.recall = recall;
        this.f_score = f_score;
    }
}