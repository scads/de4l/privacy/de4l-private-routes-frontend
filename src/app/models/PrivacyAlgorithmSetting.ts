import { PrivacyAlgorithmMetadata } from "./PrivacyAlgorithmMetadata"

export interface PrivacyAlgorithmSetting {
  parameters: number[]
  algorithm: PrivacyAlgorithmMetadata
}
