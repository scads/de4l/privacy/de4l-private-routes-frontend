
export class Stop {
    public pointId: number;
	public longitude: number;
	public latitude: number;
	public timestamp: Date;

	constructor(pointId: number, longitude: number, latitude: number, timestamp: any) {
		this.pointId = pointId;
		this.longitude = longitude;
		this.latitude = latitude;
		this.timestamp = new Date(timestamp);
	}

}

