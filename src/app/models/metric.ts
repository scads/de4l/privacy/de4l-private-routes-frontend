/** GENERAL CLASS for Metrics */
import { MetricPerformanceMeasures } from "./metric-performance-measures";

export class Metric {
    public name: string;
    public type: string; // privacy or utility
    public id: number;
    public value: number; // Percentage
    public performance?: MetricPerformanceMeasures;


    constructor(name: string, type: string, id: number, value: number, performance?: MetricPerformanceMeasures) {
        this.name = name;
        this.type = type;
        this.id = id;
        this.value = value;
        this.performance = performance;
    }
}
