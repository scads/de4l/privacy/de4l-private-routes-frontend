import { Measurement } from './measurement';
import { Metric } from './metric';

export class Geodata {
	public longitude: number;
	public latitude: number;
	public timestamp: Date;
	public id: number;
	public metrics: Metric[];
    public measurements: Measurement[];

	constructor(longitude: number, latitude: number, timestamp: any, id: number, metrics: Metric[], measurements: Measurement[] ) {
		this.longitude = longitude;
		this.latitude = latitude;
		this.timestamp = new Date(timestamp);
		this.id = id;
		this.metrics = metrics;
        this.measurements = measurements;
	}

}

