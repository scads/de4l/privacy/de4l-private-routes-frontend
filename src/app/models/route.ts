import { Geodata } from './geodata'
import { Stop } from './stop'
import { Metric } from './metric'

export class Route {
  public id: number;
  public name: string;
  public points: Geodata[];
  public date: Date;
  public metrics: Metric[];
  public stops: Stop[];

  constructor(id: number, name: string, points: Geodata[], date: Date, metrics: Metric[], stops: Stop[]) {
    this.id = id;
    this.name = name;
    this.points = points;
    this.date = date;
    this.metrics = metrics;
    this.stops = stops;
  }

}
